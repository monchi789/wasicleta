package Modelo.Repositorio;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// Creamos la clase Persistencia
public class Persistencia {

    // Creamos una instancia privada de clase de tipo de dato es de si mismo = "instancia"
    private static Persistencia instancia;

    // Definimos una variable privada de tipo EntityManager
    private EntityManager em;


    // Creamos el constructor de la clase privada
    private Persistencia() {
        // El EntityManager debe ser instanciado durante la construccion de la "instancia"
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPA_PU");

        em = emf.createEntityManager();
    }

    // Creamos un metodo get que nos permita acceder a la variable instancia
    public static Persistencia getInstancia() {

        // Definimos una condicional si la variable instancia es null
        if(instancia == null){

            // En caso de que la variable "instancia" sea null instaciamos "instancia"
            instancia = new Persistencia();
        }
        return instancia;
    }

    // Creamos el metodo get de la variable "em" de tipo EntityManager
    public EntityManager getEm() {
        return em;
    }
}
