package Modelo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "cliente") // <- Creamos la tabla cliente y la renombramos

// Notacion para los Queries
@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "Cliente", query = "select u from Cliente u order by u.idCliente")
})

// Creacion de la clase Cliente
public class Cliente {

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    // Creacion del atributo idCliente de tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idCliente")
    private Long idCliente;

    // Creacion del atributo nombreCliente de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "nombreCliente",nullable = false)
    private String nombreCliente;

    // Creacion del atributo apellidoPaternoCliente de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "apellidoPaternoCliente")
    private String apellidoPaternoCliente;

    // Creacion del atributo apellidoMaternoCliente de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "apellidoMaternoCliente")
    private String apellidoMaternoCliente;

    // Creacion del atributo tipoDocumento de tipo Stringç
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "tipoDocumento")
    private String tipoDocumento;

    // Creacion del atributo nroDocumento de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "nroDocumento")
    private String nroDocumento;

    // Creacion del atributo nroCelular de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "nroCelular")
    private String nroCelular;

    // Creacion del atributo tieneAlquiler de tipo Alquiler
    @OneToMany(mappedBy = "perteneceCliente")
    private List<Alquiler> tieneAlquiler;

    // Creacion del constructor de la clase sin parametros
    public Cliente() {
    }

    // Creacion de los metodos get y set  de idCliente
    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    // Creacion de los metodos get y set  de nombreCliente
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    // Creacion de los metodos get y set  de apellidoPaternoCliente
    public String getApellidoPaternoCliente() {
        return apellidoPaternoCliente;
    }

    public void setApellidoPaternoCliente(String apellidoPaternoCliente) {
        this.apellidoPaternoCliente = apellidoPaternoCliente;
    }

    // Creacion de los metodos get y set  de apellidoMaternoCliente
    public String getApellidoMaternoCliente() {
        return apellidoMaternoCliente;
    }

    public void setApellidoMaternoCliente(String apellidoMaternoCliente) {
        this.apellidoMaternoCliente = apellidoMaternoCliente;
    }

    // Creacion de los metodos get y set  de tipoDocumento
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    // Creacion de los metodos get y set  de nroCliente
    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    // Creacion de los metodos get y set  de nroCelular
    public String getNroCelular() {
        return nroCelular;
    }

    public void setNroCelular(String nroCelular) {
        this.nroCelular = nroCelular;
    }

    // Creacion de los metodos get y set de tieneAlquiler
    public List<Alquiler> getTieneAlquiler() {
        if(tieneAlquiler == null){
            tieneAlquiler = new ArrayList<>();
        }
        return tieneAlquiler;
    }

    public void setTieneAlquiler(List<Alquiler> tieneAlquiler) {
        this.tieneAlquiler = tieneAlquiler;
    }
}
