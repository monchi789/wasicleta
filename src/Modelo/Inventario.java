/*package Modelo;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
//ORDENA QUE ESTA CLASE SE CONVIERTA EN UNA TABLA Y QUE LOS ATRIBUTOS SEAN CAMPOS
@Entity
//NOS PERMITE ENVIAR UN ATRIBUTO COMO NAME
@Table(name = "inventario")

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "Inventario.listar", query = "select u from Inventario u order by u.idInventario")
})

//CREACION DE LA CLASE INVENTARIO
public class Inventario {

    //SELECCIONA EL CAMPO PRIMARY KEY
    @Id
    //PERMITE INDICAR QUE ID VA SER AUTONUMERICO O AUTOGENERADO
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    //CREACION DE LOS ATRIBUTOS DE CLASE

    //CREACION DEL ATRIBUTO "idInventario" de tipo Long
    //LO MISMO QUE @TABLE SOLO QUE A NIVEL DE COLUMNAS
    @Column(name = "idInventario")
    private Long idInventario;


    //atributo
    @Column(name = "LugarDelLocal",unique = true, nullable = false)
    private String LugarDelLocal;

    // Creacion del atributo tieneBicicleta de tipo Bicicleta

    @OneToMany(mappedBy = "perteneceInventario")
    private List<Bicicleta> tieneBicicleta;

    public Inventario() {
    }


    //METODO GET Y SET DE "idInventario"
    public Long getIdInventario() {
        return idInventario;
    }
    public void setIdInventario(Long idInventario) {
        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "idInventario" DE LA CLASE
        //this HACE REFERENCIA AL "idInventario" DEL ATRIBUTO
        this.idInventario = idInventario;
    }

    public String getLugarDelLocal() {
        return LugarDelLocal;
    }

    public void setLugarDelLocal(String lugarDelLocal) {
        LugarDelLocal = lugarDelLocal;
    }

    // Creacion de los metodos gete y set de tieneBicicleta
    public List<Bicicleta> getTieneBicicleta() {
        if(tieneBicicleta == null){
            tieneBicicleta = new ArrayList<>();
        }
        return tieneBicicleta;
    }



    public void setTieneBicicleta(List<Bicicleta> tieneBicicleta) {
        this.tieneBicicleta = tieneBicicleta;
    }
}
*/