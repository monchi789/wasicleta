package Modelo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//ORDENA QUE ESTA CLASE SE CONVIERTA EN UNA TABLA Y QUE LOS ATRIBUTOS SEAN CAMPOS
@Entity
//NOS PERMITE ENVIAR UN ATRIBUTO COMO NAME
@Table(name = "tipoBicicleta")

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "TipoBicicleta.listar", query = "select u from TipoBicicleta u order by u.idTipoBIcicleta")
})


//CREACION DE LA CLASE BICICLETA
public class TipoBicicleta {

    //SELECCIONA EL CAMPO PRIMARY KEY
    @Id
    //PERMITE INDICAR QUE ID VA SER AUTONUMERICO O AUTOGENERADO
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    //CREACION DE LOS ATRIBUTOS DE CLASE

    //CREACION DEL ATRIBUTO "idTipoBIcicleta" de tipo Long
    //LO MISMO QUE @TABLE SOLO QUE A NIVEL DE COLUMNAS
    @Column(name = "idTipoBIcicleta")
    private Long idTipoBIcicleta;

    //CREACION DEL ATRIBUTO "modeloBicicleta" DE TIPO Double
    @Column(name = "modeloBicicleta",unique = true, nullable = false)
    private String modeloBicicleta;

    // Creacion del atributo agrupaBicicleta de tipo Bicicleta
    @OneToMany(mappedBy = "tieneBicicleta")
    private List<Bicicleta> agrupaBicicleta;

    //CONSTRUCTOR
    public TipoBicicleta() {
    }



    //METODO GET Y SET DE "idTipoBIcicleta"
    public Long getIdTipoBIcicleta() {
        return idTipoBIcicleta;
    }
    public void setIdTipoBIcicleta(Long idTipoBIcicleta) {
        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "idTipoBIcicleta" DE LA CLASE
        //this HACE REFERENCIA AL "idTipoBIcicleta" DEL ATRIBUTO
        this.idTipoBIcicleta = idTipoBIcicleta;
    }

    //METODO GET Y SET DE "modeloBicicleta"
    public String getModeloBicicleta() {
        return modeloBicicleta;
    }
    public void setModeloBicicleta(String modeloBicicleta) {
        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "modeloBicicleta" DE LA CLASE
        //this HACE REFERENCIA AL "modeloBicicleta" DEL ATRIBUTO
        this.modeloBicicleta = modeloBicicleta;
    }

    // Metodos get y set de agrupaBicicleta
    public List<Bicicleta> getAgrupaBicicleta() {
        if(agrupaBicicleta == null){
            agrupaBicicleta = new ArrayList<>();
        }
        return agrupaBicicleta;
    }

    public void setAgrupaBicicleta(List<Bicicleta> agrupaBicicleta) {
        this.agrupaBicicleta = agrupaBicicleta;
    }
}
