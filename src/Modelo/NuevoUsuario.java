package Modelo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "nuevoUsuario") // <- Creamos la tabla cliente y la renombramos

// Notacion para los Queries
@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "NuevoUsuario.listar", query = "select u from NuevoUsuario u order by u.idNuevoUsuario")
})

// Creacion de la clase NuevoUsuario
public class NuevoUsuario {

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    // Creacion del atributo idUsuario de tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idNuevoUsuario")
    private Long idNuevoUsuario;

    // Creacion del atributo nombreUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "nombreUsuario",unique = true, nullable = false)
    private String nombreUsuario;

    // Creacion del atributo apellidosPaternosUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "apellidosPaternosUsuario")
    private String apellidosPaternosUsuario;

    // Creacion del atributo apellidosMaternosUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "apellidosMaternosUsuario")
    private String apellidosMaternosUsuario;

    // Creacion del atributo edadUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "edadUsuario")
    private String edadUsuario;

    // Creacion del atributo dniUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "dniUsuario")
    private String dniUsuario;

    // Creacion del atributo celularUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "celularusuario")
    private String celularUsuario;

    // Creacion del atributo correoUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "correoUsuario")
    private String correoUsuario;

    // Creacion del atributo direccionUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas  ç
    @Column(name = "direccionUsuario")
    private String direccionUsuario;

    // Creacion del atributo creaNuevoUsuario de tipo Usuario
    @OneToMany(mappedBy = "perteneceNuevoUsuario")
    private List<Usuario> creaUsuario;

    // Creacion del contructor de la clase sin parametros
    public NuevoUsuario() {
    }

    // Metodos get y set de idUsuario
    public Long getIdUsuario() {
        return idNuevoUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idNuevoUsuario = idUsuario;
    }


    // Metodos get y set de nombreUsuario
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    // Metodos get y set de apellidosPaternosUsuario
    public String getApellidosPaternosUsuario() {
        return apellidosPaternosUsuario;
    }

    public void setApellidosPaternosUsuario(String apellidosPaternosUsuario) {
        this.apellidosPaternosUsuario = apellidosPaternosUsuario;
    }

    // Metodos get y set de apellidosMaternosUsuario
    public String getApellidosMaternosUsuario() {
        return apellidosMaternosUsuario;
    }

    public void setApellidosMaternosUsuario(String apellidosMaternosUsuario) {
        this.apellidosMaternosUsuario = apellidosMaternosUsuario;
    }

    // Metodos get y set de edadUsuario
    public String getEdadUsuario() {
        return edadUsuario;
    }

    public void setEdadUsuario(String edadUsuario) {
        this.edadUsuario = edadUsuario;
    }

    // Metodos get y set de dniUsuario
    public String getDniUsuario() {
        return dniUsuario;
    }

    public void setDniUsuario(String dniUsuario) {
        this.dniUsuario = dniUsuario;
    }

    // Metodos get y set de celularUsuario
    public String getCelularUsuario() {
        return celularUsuario;
    }

    public void setCelularUsuario(String celularUsuario) {
        this.celularUsuario = celularUsuario;
    }

    // Metodos get y set de correoUsuario
    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    // Metodos get y set de direccionUsuario
    public String getDireccionUsuario() {
        return direccionUsuario;
    }

    public void setDireccionUsuario(String direccionUsuario) {
        this.direccionUsuario = direccionUsuario;
    }

    // Metodos get y set de CreaUsuario
    public List<Usuario> getCreaUsuario() {
        if(creaUsuario == null){

            creaUsuario = new ArrayList<>();

        }

        return creaUsuario;
    }

    public void setCreaUsuario(List<Usuario> creaUsuario) {
        this.creaUsuario = creaUsuario;
    }
}
