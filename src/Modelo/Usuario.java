package Modelo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "usuario") // <- Creamos la tabla cliente y la renombramos

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "Usuario.listar", query = "select u from Usuario u order by u.idUsuario"),
        //consulta para validar usuario
        // comparar usuario luego el password etiquetando el parametro ":"
        @NamedQuery(name = "Usuario.buscarUsuario", query = "select u from Usuario u where u.Usuario = :Usuario"),
})

// Creacion de la clase Usuario
public class Usuario {

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    // Creacion de los atributos de la clase

    // Creacion del atributo idUsuario tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idUsuario")
    private Long idUsuario;

    // Creacion del atributo codUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "usuario")
    private String  Usuario;

    // Creacion del atributo contrasenaUsuario de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "contrasenaUsuario")
    private String constrasenaUsuario;

    // Creacion del atributo perteneceNuevoUsuario de tipo NuevoUsuario
    @ManyToOne
    // Lo mismo que "@Column" solo que para los foreign keys
    @JoinColumn(name = "perteneceNuevoUsuario", referencedColumnName = "idNuevoUsuario")
    private NuevoUsuario perteneceNuevoUsuario;

    @OneToMany(mappedBy = "perteneceUsuario")
    // Creacion del atributo haceAlquiler de tipo Alquiler
    private List<Alquiler> haceAlquiler;

    // Creacion del constructor de la clase sin parametros
    public Usuario() {
    }

    // Constructor de la clase Usuario sin parametros
    public Usuario(Long idUsuario) {

        this.idUsuario = idUsuario;
    }
    //Creacion de los metodos get y set de idUsuario

    public Long getIdUsuario() {
        return idUsuario;
    }

    // Creacion de los metodos get y set de Usuario
    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    // Creacion de los metodos get y set de constraseñaUsuario
    public String getConstrasenaUsuario() {
        return constrasenaUsuario;
    }

    public void setConstrasenaUsuario(String constrasenaUsuario) {
        this.constrasenaUsuario = constrasenaUsuario;
    }

    // Creacion de los metodos get y set de perteneceNuevoUsuario
    public NuevoUsuario getPerteneceNuevoUsuario() {
        return perteneceNuevoUsuario;
    }

    public void setPerteneceNuevoUsuario(NuevoUsuario perteneceNuevoUsuario) {
        this.perteneceNuevoUsuario = perteneceNuevoUsuario;
    }

    // Creacion de los metodos get y set de haceAlquiler
    public List<Alquiler> getHaceAlquiler() {
        if(haceAlquiler == null){
            haceAlquiler = new ArrayList<>();
        }
        return haceAlquiler;
    }

    public void setHaceAlquiler(List<Alquiler> haceAlquiler) {
        this.haceAlquiler = haceAlquiler;
    }
}
