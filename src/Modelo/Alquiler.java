package Modelo;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "alquiler") // <- Creamos la tabla cliente y la renombramos

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "Alquiler.listar", query = "select u from Alquiler u order by u.idAlquiler")
})

// Creacion de la clase Alquiler
public class Alquiler {

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    //Creamos los atributos de la clase

    //Creaciòn del atributo idAlquiler de tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idAlquiler")
    private long idAlquiler;

    //Creamos atributo fechaAlquiler de tipo localdatetime
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "fechaAlquiler")
    private String fechaAlquiler;

    //Creamos atributo codigoAlquiler de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "codigoAlquiler")
    private String codigoAlquiler;

    @Column (name = "montoAlquiler")
    private String montoALquiler;



    // Creamos atributo perteneceUsuario de tipo Usuario
    @ManyToOne
    // Lo mismo que "@Column" solo que para los foreign keys
    @JoinColumn(name = "perteneceUsuario", referencedColumnName = "idUsuario")
    private Usuario perteneceUsuario;

    @ManyToOne()
    // Lo mismo que "@Column" solo que para los foreign keys
    @JoinColumn(name = "perteneceCliente", referencedColumnName = "idCliente")
    // Creacion del atributo perteneceCliente de tipo Cliente
    private Cliente perteneceCliente;



    // Lo mismo que "@Column" solo que para los foreign keys
    //@JoinColumn(name = "perteneceClienteFrecuente", referencedColumnName = "idFrecuente")
    // Creacion del atributo perteneceClienteFrecuente de tipo ClientenFrecuente
    //private ClienteFrecuente perteneceClienteFrecuente;




    @OneToMany(mappedBy = "perteneceDetalleAlquiler")
    // Creacion del atributo tieneAlquiler de tipo DetalleAlquiler
    private List<DetalleDeAlquiler> tieneAlquiler;


    //Constructor sin paràmetros
    public Alquiler() {
    }

    //Mètodos get y set de idAlquiler
    public long getIdAlquiler() {

        //Devuelve atributo idAlquiler
        return idAlquiler;
    }

    public void setIdAlquiler(long idAlquiler) {
        this.idAlquiler = idAlquiler;
    }

    //Mètodos get y set de fechaAlquiler
    public String getFechaAlquiler() {
        return fechaAlquiler;
    }

    public void setFechaAlquiler(String fechaAlquiler) {
        this.fechaAlquiler = fechaAlquiler;
    }

    //Mètodos get y set de codigoAlquiler
    public String getCodigoAlquiler() {
        return codigoAlquiler;
    }

    public void setCodigoAlquiler(String codigoAlquiler) {
        this.codigoAlquiler = codigoAlquiler;
    }

    //
    public String getMontoALquiler() {
        return montoALquiler;
    }

    public void setMontoALquiler(String montoALquiler) {
        this.montoALquiler = montoALquiler;
    }

    // Creacion de los metodos get y set de perteneceUsuario
    public Usuario getPerteneceUsuario() {
        return perteneceUsuario;
    }

    public void setPerteneceUsuario(Usuario perteneceUsuario) {
        this.perteneceUsuario = perteneceUsuario;
    }

    // Creacion de los metodos get y set de perteneceCliente
    public Cliente getPerteneceCliente() {
        return perteneceCliente;
    }

    public void setPerteneceCliente(Cliente perteneceCliente) {
        this.perteneceCliente = perteneceCliente;
    }

    // Creacion de los metodos get y set de perteneceClienteFrecuente
    //public ClienteFrecuente getPerteneceClienteFrecuente() {
    //    return perteneceClienteFrecuente;
    // }

    //public void setPerteneceClienteFrecuente(ClienteFrecuente perteneceClienteFrecuente) {
    //    this.perteneceClienteFrecuente = perteneceClienteFrecuente;
    // }



    // Creacion de los metodos get y set de tieneAlquiler
    public List<DetalleDeAlquiler> getTieneAlquiler() {
        if(tieneAlquiler == null){
            tieneAlquiler = new ArrayList<>();
        }
        return tieneAlquiler;
    }

    public void setTieneAlquiler(List<DetalleDeAlquiler> tieneAlquiler) {
        this.tieneAlquiler = tieneAlquiler;
    }
}
