/*package Modelo;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "clienteFrecuente") // <- Creamos la tabla cliente y la renombramos
public class ClienteFrecuente extends Cliente{

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    // Creacion del atributo idFrecuente de tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idFrecuente")
    private Long idFrecuente;

    // Creacion del atributo codigoFrecuente de tipo String
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "codigoFrecuente")
    private String codigoFrecuente;

    @OneToMany(mappedBy = "perteneceClienteFrecuente")
    private List<Alquiler> tieneAlquilerFrecuente;

    // Creacion del constructor de la clase sin parametros
    public ClienteFrecuente() {
    }

    // Sobreescribimos los metodos get y set de idCliente
    @Override
    public long getIdCliente() {
        return super.getIdCliente();
    }

    @Override
    public void setIdCliente(Long idCliente) {
        super.setIdCliente(idCliente);
    }

    // Sobreescribimos los metodos get y set de nombreCliente
    @Override
    public String getNombreCliente() {
        return super.getNombreCliente();
    }

    @Override
    public void setNombreCliente(String nombreCliente) {
        super.setNombreCliente(nombreCliente);
    }

    // Sobreescribimos los metodos get y set de apellidoPaternoCliente
    @Override
    public String getApellidoPaternoCliente() {
        return super.getApellidoPaternoCliente();
    }

    @Override
    public void setApellidoPaternoCliente(String apellidoPaternoCliente) {
        super.setApellidoPaternoCliente(apellidoPaternoCliente);
    }

    // Sobreescribimos los metodos get y set de idCliente
    @Override
    public String getApellidoMaternoCliente() {
        return super.getApellidoMaternoCliente();
    }

    @Override
    public void setApellidoMaternoCliente(String apellidoMaternoCliente) {
        super.setApellidoMaternoCliente(apellidoMaternoCliente);
    }

    // Sobreescribimos los metodos get y set de tipoDocumento
    @Override
    public String getTipoDocumento() {
        return super.getTipoDocumento();
    }

    @Override
    public void setTipoDocumento(String tipoDocumento) {
        super.setTipoDocumento(tipoDocumento);
    }

    // Sobreescribimos los metodos get y set de nroDocumento
    @Override
    public String getNroDocumento() {
        return super.getNroDocumento();
    }

    @Override
    public void setNroDocumento(String nroDocumento) {
        super.setNroDocumento(nroDocumento);
    }

    // Sobreescribimos los metodos get y set de nroCelular
    @Override
    public String getNroCelular() {
        return super.getNroCelular();
    }

    @Override
    public void setNroCelular(String nroCelular) {
        super.setNroCelular(nroCelular);
    }

    // Sobreescribimos los metodos get y set de tieneAlquiler
    @Override
    public List<Alquiler> getTieneAlquiler() {
        return super.getTieneAlquiler();
    }

    @Override
    public void setTieneAlquiler(List<Alquiler> tieneAlquiler) {
        super.setTieneAlquiler(tieneAlquiler);
    }

    // Creacion de los metodos get y set de idFrecuente
    public Long getIdFrecuente() {
        return idFrecuente;
    }

    public void setIdFrecuente(Long idFrecuente) {
        this.idFrecuente = idFrecuente;
    }

    // Creacion de los metodos get y set de codigoFrecuente
    public String getCodigoFrecuente() {
        return codigoFrecuente;
    }

    public void setCodigoFrecuente(String codigoFrecuente) {
        this.codigoFrecuente = codigoFrecuente;
    }

    public List<Alquiler> getTieneAlquilerFrecuente() {
        return tieneAlquilerFrecuente;
    }

    public void setTieneAlquilerFrecuente(List<Alquiler> tieneAlquilerFrecuente) {
        this.tieneAlquilerFrecuente = tieneAlquilerFrecuente;
    }
}
*/