package Modelo;

import javax.persistence.*;

@Entity // <- Hace que la clase NuevoUsuario se vuelva una tabla y hace que los atributos se vuelvan en campos

@Table(name = "detalleDeAlquiler") // <- Creamos la tabla cliente y la renombramos

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "DetalleAlquiler.listar", query = "select u from DetalleDeAlquiler u order by u.idDetalleAlquiler")
})


// Creacion de la clase DetalleDeAlquiler
public class DetalleDeAlquiler {

    // Selecciona el campo primary key
    @Id

    // Permite indicar que id va ser autonumerico o autogenerado
    @GeneratedValue(strategy =  GenerationType.IDENTITY)

    // Creacion del atributo idDetalleAlquiler de tipo Long
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "idDetalleAlquiler")
    private Long idDetalleAlquiler;

    // Creacion del atributo descuentoDetalleAlquiler de tipo Double
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "descuentoDetalleAlquiler")
    private String descuentoDetalleAlquiler;

    // Creacion del atributo fechaDetalleAlquiler de tipo LocalDateTime
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "fechaDetalleAlquiler")
    private String fechaDetalleAlquiler;

    // Creacion del atributo cantidadDetalleAlquiler de tipo int
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "cantidadDetalleAlquiler")
    private String cantidadDetalleAlquiler;

    // Creacion del atributo totalDelAlquiler de tipo Double
    // Lo mismo que "@Table" solo que a nivel de columnas
    @Column(name = "totalDelAlquiler")
    private String totalDelAlquiler;

    // Creacion del atributo refiereBicicleta de tipo Bicicleta
    @ManyToOne
    @JoinColumn(name = "refiereBicicleta", referencedColumnName = "idBicicleta")
    private Bicicleta refiereBicicleta;

    // Creacion del atributo perteneceDetalleAlquiler de tipo Alquiler
    @ManyToOne
    @JoinColumn(name = "perteneceDetalleAlquiler", referencedColumnName = "idAlquiler")
    private Alquiler perteneceDetalleAlquiler;

    // Creacion del constructor de la clase sin parametros
    public DetalleDeAlquiler() {
    }

    // Creacion de los metodos get y set idDetalleAlquiler
    public long getIdDetalleAlquiler() {
        return idDetalleAlquiler;
    }

    public void setIdDetalleAlquiler(Long idDetalleAlquiler) {
        this.idDetalleAlquiler = idDetalleAlquiler;
    }

    // Creacion de los metodos get y set getDescuentoDetalleAlquiler
    public String getDescuentoDetalleAlquiler() {
        return descuentoDetalleAlquiler;
    }

    public void setDescuentoDetalleAlquiler(String descuentoDetalleAlquiler) {
        this.descuentoDetalleAlquiler = descuentoDetalleAlquiler;
    }

    // Creacion de los metodos get y set fechaDetalleAlquiler
    public String getFechaDetalleAlquiler() {
        return fechaDetalleAlquiler;
    }

    public void setFechaDetalleAlquiler(String fechaDetalleAlquiler) {
        this.fechaDetalleAlquiler = fechaDetalleAlquiler;
    }

    // Creacion de los metodos get y set cantidadDetalleAlquiler
    public String getCantidadDetalleAlquiler() {
        return cantidadDetalleAlquiler;
    }

    public void setCantidadDetalleAlquiler(String cantidadDetalleAlquiler) {
        this.cantidadDetalleAlquiler = cantidadDetalleAlquiler;
    }

    // Creacion de los metodos get y set totalDelAlquiler
    public String getTotalDelAlquiler() {
        return totalDelAlquiler;
    }

    public void setTotalDelAlquiler(String totalDelAlquiler) {
        this.totalDelAlquiler = totalDelAlquiler;
    }

    // Creacion de los metodos get y set de refiereBicicleta
    public Bicicleta getRefiereBicicleta() {
        return refiereBicicleta;
    }

    public void setRefiereBicicleta(Bicicleta refiereBicicleta) {
        this.refiereBicicleta = refiereBicicleta;
    }

    // Creacion de los metodos get y set de perteneceDetalleAlquiler
    public Alquiler getPerteneceDetalleAlquiler() {
        return perteneceDetalleAlquiler;
    }

    public void setPerteneceDetalleAlquiler(Alquiler perteneceDetalleAlquiler) {
        this.perteneceDetalleAlquiler = perteneceDetalleAlquiler;
    }
}
