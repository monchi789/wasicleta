package Modelo;

import javax.persistence.*;

//ORDENA QUE ESTA CLASE SE CONVIERTA EN UNA TABLA Y QUE LOS ATRIBUTOS SEAN CAMPOS
import java.util.ArrayList;
import java.util.List;

@Entity
//NOS PERMITE ENVIAR UN ATRIBUTO COMO NAME
@Table(name = "bicicleta")

@NamedQueries({
        // Definimos una consulta
        @NamedQuery(name = "Bicicleta.listar", query = "select u from Bicicleta u order by u.idBicicleta")
})

//CREACION DE LA CLASE BICICLETA
public class Bicicleta {

    //SELECCIONA EL CAMPO PRIMARY KEY
    @Id
    //PERMITE INDICAR QUE ID VA SER AUTONUMERICO O AUTOGENERADO
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //CREACION DE LOS ATRIBUTOS DE CLASE
    //CREACION DEL ATRIBUTO "idBicicleta" de tipo Long
    //LO MISMO QUE @TABLE SOLO QUE A NIVEL DE COLUMNAS
    @Column(name = "idBicicleta")
    private Long idBicicleta;

    //CREACION DEL ATRIBUTO "precioBicicleta" DE TIPO Double
    @Column(name = "precioBicicleta")
    private String precioBicicleta;

    //CREACION DEL ATRIBUTO "codigoBicicleta" DE TIPO String
    @Column(name = "codigoBicicleta")
    private String codigoBicicleta;

    //CREACION DEL ATRIBUTO "estadoBicicleta" DE TIPO String
    @Column(name = "estadoBicicleta")
    private String estadoBicicleta;


    //Creacion del atributo tieneTipoBicicleta de tipo Bicicleta
    @ManyToOne
    @JoinColumn(name = "tieneBicicleta", referencedColumnName = "idTipoBicicleta")
    private TipoBicicleta tieneBicicleta;

    // Creacion del atributo perteneceInventario de tipo Inventario
 /*   @ManyToOne
    @JoinColumn(name = "perteneceInventario", referencedColumnName = "idInventario")
    private Inventario perteneceInventario;*/

    // Creacion del atributo esReferidoDetalleAlquiler de tipo DetalleDeAlquiler
    @OneToMany(mappedBy = "refiereBicicleta")
    private List<DetalleDeAlquiler> esReferidoDetalleAlquiler;

    //CONSTRUCTOR
    public Bicicleta() {
    }

    //METODO GET Y SET DE "idBicicleta"
    public Long getIdBicicleta() {
        return idBicicleta;
    }
    public void setIdBicicleta(Long idBicicleta) {

        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "idBicicleta" DE LA CLASE
        //this HACE REFERENCIA AL "idBicicleta" DEL ATRIBUTO
        this.idBicicleta = idBicicleta;
    }

    //METODO GET Y SET DE "precioBicicleta"
    public String getPrecioBicicleta() {
        return precioBicicleta;
    }
    public void setPrecioBicicleta(String precioBicicleta) {

        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "precioBicicleta" DE LA CLASE
        //this HACE REFERENCIA AL "precioBicicleta" DEL ATRIBUTO
        this.precioBicicleta = precioBicicleta;
    }

    //METODO GET Y SET DE "codigoBicicleta"
    public String getCodigoBicicleta() {
        return codigoBicicleta;
    }
    public void setCodigoBicicleta(String codigoBicicleta) {

        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "codigoBicicleta" DE LA CLASE
        //this HACE REFERENCIA AL "codigoBicicleta" DEL ATRIBUTO
        this.codigoBicicleta = codigoBicicleta;
    }


    //METODO GET Y SET DE "estadoBicicleta"
    public String getEstadoBicicleta() {
        return estadoBicicleta;
    }
    public void setEstadoBicicleta(String estadoBicicleta) {

        //ASIGNA EL VALOR DEL PARAMETRO DE ENTRADA HACIA EL ATRIBUTO "estadoBicicleta" DE LA CLASE
        //this HACE REFERENCIA AL "estadoBicicleta" DEL ATRIBUTO
        this.estadoBicicleta = estadoBicicleta;
    }

    // Creacion de los metodos get y set de tieneBicicleta
    public TipoBicicleta getTieneBicicleta() {
        return tieneBicicleta;
    }

    public void setTieneBicicleta(TipoBicicleta tieneBicicleta) {
        this.tieneBicicleta = tieneBicicleta;
    }

/*    // Creacion de los metodos get y set de perteneceInventario
    public Inventario getPerteneceInventario() {
        return perteneceInventario;
    }

    public void setPerteneceInventario(Inventario perteneceInventario) {
        this.perteneceInventario = perteneceInventario;
    }*/

    // Creacion de los metodos get y set de esReferidoDetalleAlquiler
    public List<DetalleDeAlquiler> getEsReferidoDetalleAlquiler() {

        if(esReferidoDetalleAlquiler == null){
            esReferidoDetalleAlquiler = new ArrayList<>();
        }

        return esReferidoDetalleAlquiler;
    }

    public void setEsReferidoDetalleAlquiler(List<DetalleDeAlquiler> esReferidoDetalleAlquiler) {
        this.esReferidoDetalleAlquiler = esReferidoDetalleAlquiler;
    }
}
