package Control;

import Modelo.Repositorio.Persistencia;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.persistence.Persistence;

public class Wasicleta extends Application {

    // Creamos dos variables para poder almacernar las posiciones en X y Y
    private double posX = 0;
    private double posY = 0;

    public static void main(String[] args) {

        // EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPA_PU");
        // EntityManager em = emf.createEntityManager();

        /*llamamos a la clase persistencia con Persistencia por eso se usa Static dentro de la clase
         Static se inicia solo una vez al inicio de ejecutar el programa*/
        Persistencia.getInstancia();

        // Invocamos el metodo start
        launch();

    }

    @Override
    public void start(Stage stage) throws Exception {

        // Creamos una variable de tipo parent donde ponemos la direccion del archivo fxml
        Parent root = FXMLLoader.load(getClass().getResource("/Vista/login.fxml"));

        // Creamos un objeto de tipo scena y pasamos por parametro el root
        Scene scene = new Scene(root);

        // Definimos un estilo de escenario sin fondo ni decoraciones
        stage.initStyle(StageStyle.TRANSPARENT);

        // Definimos el color del fondo como transparente
        scene.setFill(Color.TRANSPARENT);

        // El evento "setOnMousePressed" se llama solo cuando es presionado un boton del mouse
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                // Aqui asignamos la posicion actual que tiene la aplicacion en X y Y
                posX = event.getSceneX();
                posY = event.getSceneY();

                // getSceneX() -> Returns horizontal position of the event relative to the origin of the Scene that contains the MouseEvent's source.
                // getSceneY() -> Returns vertical position of the event relative to the origin of the Scene that contains the MouseEvent's source.

            }
        });

        // El evento "setOnMouseDragged" se llama solo cuando el mouse se mueve con un boton presionado
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                // Asignamos la nueva posicion, obteniendo la posicion actual y restandolo con la posicion de inicio
                stage.setX(event.getScreenX() - posX);
                stage.setY(event.getScreenY() - posY);
            }
        });

        // Ponemos la scena en pantalla
        stage.setScene(scene);

        // Mostramos la scena en la pantalla
        stage.show();

    }
}
