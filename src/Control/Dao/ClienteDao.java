package Control.Dao;

import Modelo.Cliente;
import Modelo.NuevoUsuario;
import Modelo.Repositorio.Persistencia;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/*creamos una clase de tipo Data Accepts Objects "Dao"*/
public class ClienteDao {


    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<Cliente> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("Cliente");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un cliente
    public static void crear(Cliente nuevoCliente) {

        /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(nuevoCliente);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

    }

    public static void editar(Cliente editarCliente){

        em.getTransaction().begin();
        em.merge(editarCliente);
        em.getTransaction().commit();

    }


    public static void eliminar(Cliente eliminarCliente){
        em.getTransaction().begin();
        em.remove(eliminarCliente);
        em.getTransaction().commit();
    }
}
