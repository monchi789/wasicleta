package Control.Dao.DaoAlquiler;

import Control.Dao.DaoUsuario.UsuarioDao;
import Modelo.*;
import Modelo.Repositorio.Persistencia;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class AlquilerDao {
    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<Alquiler> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("Alquiler.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un cliente
    public static void crear(Alquiler crearAlquiler) {

        /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(crearAlquiler);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

        /*//foreing key
        Cliente cliente = crearAlquiler.getPerteneceCliente();
        cliente.getTieneAlquiler().add(crearAlquiler);*/


        //datos foraneos cliente
        Cliente cliente = crearAlquiler.getPerteneceCliente();
        //getTieneAlquiler sale del get del modelo de las clases sett&getter
        //cliente.getTieneAlquiler().add(crearAlquiler);
        cliente.getTieneAlquiler().add(crearAlquiler);

        //datos foraneos Usuario
        Usuario usuario = crearAlquiler.getPerteneceUsuario();
        //getTieneAlquiler sale del get del modelo de las clases sett&getter de la clase usuario
        usuario.getHaceAlquiler().add(crearAlquiler);

        /*//datos foraneos PerteneceHistoria
        HistorialAlquiler historialAlquiler = crearAlquiler.getPerteneceHistorialAlquiler();
        //getTieneAlquiler sale del get del modelo de las clases sett&getter de la clase HistorialAlquiler
        historialAlquiler.getTieneAlquiler().add(crearAlquiler);*/




    }

    public static void editar(Alquiler editarAlquiler){

        em.getTransaction().begin();
        em.merge(editarAlquiler);
        em.getTransaction().commit();

    }


    public static void eliminar(Alquiler eliminarAlquiler){
        em.getTransaction().begin();
        em.remove(eliminarAlquiler);
        em.getTransaction().commit();
    }
}
