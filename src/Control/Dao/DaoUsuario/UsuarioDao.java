package Control.Dao.DaoUsuario;

import Modelo.Cliente;
import Modelo.NuevoUsuario;
import Modelo.Repositorio.Persistencia;
import Modelo.Usuario;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public class UsuarioDao {
    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<Usuario> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("Usuario.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    //devolvemos un opjeto de tipo usuario de la clase
    public static Usuario buscarUsuario(String usuario) {
        Query consulta = em.createNamedQuery("Usuario.buscarUsuario");
        consulta.setParameter("Usuario", usuario);
        Usuario salida = null;
        //return (Usuario) consulta.getSingleResult();
        try {
            salida = (Usuario) consulta.getSingleResult();
        } catch(NoResultException e) {
            System.out.println(e.getMessage());
        }
        return  salida;
    }




    // Metodo para crear un cliente
    public static void crear(Usuario crearUsuario) {

        /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(crearUsuario);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

        //foreing key
        NuevoUsuario nuevoUsuario = crearUsuario.getPerteneceNuevoUsuario();
        nuevoUsuario.getCreaUsuario().add(crearUsuario);

    }

    public static void editar(Usuario editarUsuario){

        em.getTransaction().begin();
        em.merge(editarUsuario);
        em.getTransaction().commit();

    }


    public static void eliminar(Usuario eliminarUsuario){
        em.getTransaction().begin();
        em.remove(eliminarUsuario);
        em.getTransaction().commit();
    }
}
