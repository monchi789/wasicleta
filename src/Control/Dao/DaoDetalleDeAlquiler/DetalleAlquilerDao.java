package Control.Dao.DaoDetalleDeAlquiler;

import Modelo.*;
import Modelo.Repositorio.Persistencia;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class DetalleAlquilerDao {
    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<DetalleDeAlquiler> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("DetalleAlquiler.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un cliente
    public static void crear(DetalleDeAlquiler crearDetalleDeAlquiler) {

        /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(crearDetalleDeAlquiler);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

      /*  //clave foranea
        TipoBicicleta tipoBicicleta = crearBicicleta.getTieneBicicleta();
        tipoBicicleta.getAgrupaBicicleta().add(crearBicicleta);*/

        //datos foraneos
        Bicicleta bicicleta = crearDetalleDeAlquiler.getRefiereBicicleta();
        //getEsRegerido sale del get del modelo de las clases sett&getter
        bicicleta.getEsReferidoDetalleAlquiler().add(crearDetalleDeAlquiler);


        //clave foranea
        Alquiler alquiler = crearDetalleDeAlquiler.getPerteneceDetalleAlquiler();
        //getTieneAlquiler sale del get del modelo de las clases sett&getter
        alquiler.getTieneAlquiler().add(crearDetalleDeAlquiler);

    }

    public static void editarDetalle(DetalleDeAlquiler editarDetalleDeAlquiler){

        em.getTransaction().begin();
        em.merge(editarDetalleDeAlquiler);
        em.getTransaction().commit();

    }


    public static void eliminarDetalleA(DetalleDeAlquiler eliminarDetalleDeAlquiler){
        em.getTransaction().begin();
        em.remove(eliminarDetalleDeAlquiler);
        em.getTransaction().commit();
    }
}
