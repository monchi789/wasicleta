package Control.Dao.DaoBicicleta;

import Modelo.Alquiler;
import Modelo.Bicicleta;
import Modelo.NuevoUsuario;
import Modelo.Repositorio.Persistencia;
import Modelo.TipoBicicleta;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class BicicletaDao {
    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<Bicicleta>listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("Bicicleta.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un cliente
    public static void crear(Bicicleta crearBicicleta) {

        /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(crearBicicleta);

        // Grabamos los datos con un commit
        em.getTransaction().commit();


        //clave foranea
        TipoBicicleta tipoBicicleta = crearBicicleta.getTieneBicicleta();
        tipoBicicleta.getAgrupaBicicleta().add(crearBicicleta);


/*
        TipoBicicleta tipoBicicleta = crearBicicleta.getTieneBicicleta();
        tipoBicicleta.getAgrupaBicicleta().add(crearBicicleta);*/


    }

    public static void editarBici(Bicicleta editarBicicleta){

        em.getTransaction().begin();
        em.merge(editarBicicleta);
        em.getTransaction().commit();

    }


    public static void eliminarBici(Bicicleta eliminarBicicleta){
        em.getTransaction().begin();
        em.remove(eliminarBicicleta);
        em.getTransaction().commit();
    }
}
