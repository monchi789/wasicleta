package Control.Dao.daoTipoBicicleta;

import Modelo.Cliente;
import Modelo.Repositorio.Persistencia;
import Modelo.TipoBicicleta;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TipoBicicletaDao {


    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<TipoBicicleta> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("TipoBicicleta.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un tipo bicicleta


    public static void crearTipoBici(TipoBicicleta tipoBicicleta) {

         /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(tipoBicicleta);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

    }

    public static void editarTipoBici(TipoBicicleta editarTipoBicicleta){

        em.getTransaction().begin();
        em.merge(editarTipoBicicleta);
        em.getTransaction().commit();

    }


    public static void eliminar(TipoBicicleta eliminarTipoBicicleta){
        em.getTransaction().begin();
        em.remove(eliminarTipoBicicleta);
        em.getTransaction().commit();
    }
}
