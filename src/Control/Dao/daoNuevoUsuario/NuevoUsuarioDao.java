package Control.Dao.daoNuevoUsuario;

import Modelo.NuevoUsuario;
import Modelo.Repositorio.Persistencia;
import Modelo.TipoBicicleta;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class NuevoUsuarioDao {

    // Creamos un atributo de tipo EntityManager y le asignamos su valor
    private static EntityManager em = Persistencia.getInstancia().getEm();

    // Creamos un metodo de tipo lista para poder listar
    public static List<NuevoUsuario> listar(){
        // Ejecutamos el Query y la llamamos desde el EntityManager
        Query consulta = em.createNamedQuery("NuevoUsuario.listar");

        // Devolvemos la consulta
        return consulta.getResultList();
    }

    // Metodo para crear un tipo bicicleta


    public static void crearNuevoU(NuevoUsuario nuevoNuevoUsuario) {

         /*insertamos informacion a la base de datos y mediado con una transaccion "getTransaction"
        y se debe iniciar "begin"*/
        em.getTransaction().begin();

        // Persistimos
        em.persist(nuevoNuevoUsuario);

        // Grabamos los datos con un commit
        em.getTransaction().commit();

    }

    public static void editarNuevoU(NuevoUsuario editarNuevoUsuario){

        em.getTransaction().begin();
        em.merge(editarNuevoUsuario);
        em.getTransaction().commit();

    }


    public static void eliminar(NuevoUsuario eliminarNuevoUsuario){
        em.getTransaction().begin();
        em.remove(eliminarNuevoUsuario);
        em.getTransaction().commit();
    }

}
