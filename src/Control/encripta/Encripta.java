package Control.encripta;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

//clase para encriptar contrasena
public class Encripta {

    public static String encripta(String original){
        String salida = "";
        //md5,SHA-512 nombre del algoritmo de encriptacion
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");

        }catch (NoSuchAlgorithmException e){
            System.out.printf(e.getMessage());

        }
        byte[] dig = md.digest(original.getBytes(StandardCharsets.UTF_8));
        salida = conversor(dig);
        return salida;
    }

    private static String conversor(byte[] arreglo) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < arreglo.length; i++){
            hexStringBuffer.append(byteToHex(arreglo[i]));
        }
        return hexStringBuffer.toString();
    }

    private static String byteToHex(byte numero) {
        char[] hexDigestion = new char[2];
        hexDigestion[0] = Character.forDigit((numero>>4) & 0xF, 16);
        hexDigestion[1] = Character.forDigit((numero & 0xF), 16);
        return new String(hexDigestion);
    }
}
