package Control.Vista.alquilerBiciceltasDatos;

import Control.Dao.ClienteDao;
import Control.Dao.DaoAlquiler.AlquilerDao;
import Control.Vista.LoginController;
import Modelo.Alquiler;
import Modelo.Cliente;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

public class AlquilerController implements Initializable {

    public String codigo = UUID.randomUUID().toString().toUpperCase(Locale.ROOT).substring(0,6);

    //comboBox
    public ComboBox<String> cmb_tipoDocumento;
    //lista para el comboBox
    ObservableList<String> list = FXCollections.observableArrayList("Pasaporte","DNI","libreta militar","Licencia de conducir");
    //
    public ComboBox cmb_tipoBicicleta;
    ObservableList<String> TipoBicicleta = FXCollections.observableArrayList("BMX","Montañera","Ciudad","Ruta","Profesional urbano");
    public ComboBox cmb_IdBicicleta;


    //cliente
    public TextField txt_nombreCliente;
    public TextField txt_apellidoPaternoCliente;
    public TextField txt_apellidoMaternoCliente;
    public TextField txt_nroDocumentoCliente;
    public TextField txt_tipoDocumentoCliente;
    public TextField txt_telefonoCliente;
    //alquiler
    public DatePicker date_fechaAlquiler;
    public TextField txt_montoAlquilar;
    //boton de aceptar alquiler
    public Button btn_alquilarFinal;



    // Creamos una variable de tipo alquiler final
    private Alquiler alquiler;
    private Cliente cliente;

    //LoginController loginController = new LoginController();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_alquilarFinal.disableProperty().bind(
                txt_telefonoCliente.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_nombreCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_apellidoMaternoCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                        txt_apellidoPaternoCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                                txt_nroDocumentoCliente.textProperty().length().lessThanOrEqualTo(0)
                                        )
                                )
                        )
                ));
        cargarDatos();
    }
    //cargar Datos de comboBox
    private void cargarDatos() {
        cmb_tipoDocumento.setItems(list);
        cmb_tipoBicicleta.setItems(TipoBicicleta);
    }

    public void act_alquilarFinal(ActionEvent actionEvent) {
        if(cliente == null & alquiler == null){


            alquiler = new Alquiler();
            cliente = new Cliente();

            cliente.setNombreCliente(txt_nombreCliente.getText());
            cliente.setApellidoPaternoCliente(txt_apellidoPaternoCliente.getText());
            cliente.setApellidoMaternoCliente(txt_apellidoMaternoCliente.getText());
            cliente.setTipoDocumento(txt_tipoDocumentoCliente.getText());
            //cliente.setNroDocumento(txt_TipoCliente.getText());
            cliente.setNroDocumento(txt_nroDocumentoCliente.getText());
            cliente.setNroCelular(txt_telefonoCliente.getText());

            //alquiler.setFechaAlquiler(date_fechaAlquiler.getText());
            alquiler.setCodigoAlquiler(codigo);
            alquiler.setMontoALquiler(txt_montoAlquilar.getText());



            //foraneos autocompletar
            //alquiler.setPerteneceHistorialAlquiler();
            //alquiler.setPerteneceCliente();
            //alquiler.setPerteneceUsuario(LoginController.getTxtUsuario);

           // AlquilerDao.crear(alquiler);
            ClienteDao.crear(cliente);

        }else {
            cliente.setNombreCliente(txt_nombreCliente.getText());
            cliente.setApellidoPaternoCliente(txt_apellidoPaternoCliente.getText());
            cliente.setApellidoMaternoCliente(txt_apellidoMaternoCliente.getText());
            //cliente.setNroDocumento(txt_TipoCliente.getText());
            cliente.setTipoDocumento(txt_tipoDocumentoCliente.getText());
            cliente.setNroDocumento(txt_nroDocumentoCliente.getText());
            cliente.setNroCelular(txt_telefonoCliente.getText());

            ClienteDao.editar(cliente);
        }
    }
}
