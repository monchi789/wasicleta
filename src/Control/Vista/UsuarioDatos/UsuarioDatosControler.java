package Control.Vista.UsuarioDatos;

import Control.Dao.DaoUsuario.UsuarioDao;
import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;
import Control.Vista.CRUD.crudUsuario.MantenimientoUsuarioDetalleCRUDController;
import Control.encripta.Encripta;
import Modelo.NuevoUsuario;
import Modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class UsuarioDatosControler implements Initializable {
    //campos de la tabla de nuevoUsuario
    public TextField txt_nombreNuevoUsuario;
    public TextField txt_telefonoNuevoUsuario;
    public TextField txt_apellidoPaternoNuevoUsuario;
    public TextField txt_apellidoMaternoNuevoUsuario;
    public TextField txt_correoNuevoUsuario;
    public TextField txt_nroDocumentoNuevoUsuario123;
    public TextField txt_direccionNuevoUsuario;

    public Button btn_crearUsuario;
    //campos para Usuario y password
    public TextField txt_usuario;
    public TextField txt_password;
    public AnchorPane ach_box;
    public TextField txt_edadNuevoUsuario;

    private NuevoUsuario nuevoUsuario;
    private Usuario usuario;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        /*btn_crearUsuario.disableProperty().bind(
                txt_nombreNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_telefonoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_apellidoMaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                        txt_apellidoPaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                txt_correoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                        txt_nroDocumentoNuevoUsuario123.textProperty().length().lessThanOrEqualTo(0).or(
                                                                txt_direccionNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                                        txt_usuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                                                txt_password.textProperty().length().lessThanOrEqualTo(0)
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                ));*/
        btn_crearUsuario.disableProperty().bind(
                txt_nombreNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_telefonoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_apellidoMaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                        txt_apellidoPaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                txt_correoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                        txt_nroDocumentoNuevoUsuario123.textProperty().length().lessThanOrEqualTo(0).or(
                                                                txt_direccionNuevoUsuario.textProperty().length().lessThanOrEqualTo(0)
                                                        )
                                                )
                                        )
                                )
                        )
                ));

        cargarDatos();

    }

    private void cargarDatos() {


    }

    public void act_crearUsuario(ActionEvent actionEvent) {

        //if(nuevoUsuario == null & usuario == null){
        if(nuevoUsuario == null){

            nuevoUsuario = new NuevoUsuario();

            //usuario = new Usuario();
            //campos nuevoUsuario
            nuevoUsuario.setNombreUsuario(txt_nombreNuevoUsuario.getText());
            nuevoUsuario.setApellidosPaternosUsuario(txt_apellidoPaternoNuevoUsuario.getText());
            nuevoUsuario.setApellidosMaternosUsuario(txt_apellidoMaternoNuevoUsuario.getText());
            nuevoUsuario.setEdadUsuario(txt_edadNuevoUsuario.getText());
            nuevoUsuario.setCelularUsuario(txt_telefonoNuevoUsuario.getText());
            nuevoUsuario.setCorreoUsuario(txt_correoNuevoUsuario.getText());
            nuevoUsuario.setDniUsuario(txt_nroDocumentoNuevoUsuario123.getText());
            nuevoUsuario.setDireccionUsuario(txt_direccionNuevoUsuario.getText());
            //campos usuario
            //usuario.setUsuario(txt_usuario.getText());
            //usuario.setConstrasenaUsuario(Encripta.encripta(txt_password.getText()));

            //combo box
            //usuario.setPerteneceNuevoUsuario(txt_nroDocumentoNuevoUsuario123.getText());

            //UsuarioDao.crear(usuario);
            NuevoUsuarioDao.crearNuevoU(nuevoUsuario);

        }else {
            nuevoUsuario.setNombreUsuario(txt_nombreNuevoUsuario.getText());
            nuevoUsuario.setApellidosPaternosUsuario(txt_apellidoPaternoNuevoUsuario.getText());
            nuevoUsuario.setApellidosMaternosUsuario(txt_apellidoMaternoNuevoUsuario.getText());
            nuevoUsuario.setEdadUsuario(txt_edadNuevoUsuario.getText());
            nuevoUsuario.setCelularUsuario(txt_telefonoNuevoUsuario.getText());
            nuevoUsuario.setCorreoUsuario(txt_correoNuevoUsuario.getText());
            nuevoUsuario.setDniUsuario(txt_nroDocumentoNuevoUsuario123.getText());
            nuevoUsuario.setDireccionUsuario(txt_direccionNuevoUsuario.getText());
            //campos usuario
            //usuario.setUsuario(txt_usuario.getText());
            //usuario.setConstrasenaUsuario(Encripta.encripta(txt_password.getText()));
            //UsuarioDao.crear(usuario);

            NuevoUsuarioDao.editarNuevoU(nuevoUsuario);
        }

        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/usuarioCrearCRUD.fxml");

        Stage stage = (Stage)datos.get("escenario");

        // Indicamos que se muestre la scena y espere
        stage.showAndWait();

        ach_box.getChildren().clear();

    }

    public void act_cancelarUsuarioP(ActionEvent actionEvent) {

        ach_box.getChildren().clear();

    }

    private Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoUsuarioDetalleCRUDController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }


}
