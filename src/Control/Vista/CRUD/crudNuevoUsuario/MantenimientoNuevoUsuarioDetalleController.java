package Control.Vista.CRUD.crudNuevoUsuario;

import Control.Dao.ClienteDao;
import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;
import Modelo.Cliente;
import Modelo.NuevoUsuario;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MantenimientoNuevoUsuarioDetalleController implements Initializable {

    //campos de la talba
    public TextField txt_IDNuevoUsuario;
    public TextField txt_nombreNuevoUsuario;
    public TextField txt_apellidoPaternoNuevoUsuario;
    public TextField txt_apellidoMaternoNuevoUsuario;
    public TextField txt_edadNuevoUsuario;
    public TextField txt_celularNuevoUsuario;
    public TextField txt_correoNuevoUsuario;
    public TextField txt_nroDniNuevoUsuario;
    public TextField txt_direccionNuevoUsuario;

    //boton aceptar de nuevoUsuario
    public Button btn_aceptarNuevoUsuario;

    // Creamos una variable de tipo Cliente
    private NuevoUsuario nuevoUsuario;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        txt_IDNuevoUsuario.setDisable(true);

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_aceptarNuevoUsuario.disableProperty().bind(
                txt_nombreNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_apellidoPaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_apellidoMaternoNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                        txt_edadNuevoUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                                txt_nroDniNuevoUsuario.textProperty().length().lessThanOrEqualTo(0)
                                        )
                                )
                        )
                ));

    }


    public void aceptarNuevoUsuario(ActionEvent actionEvent) {

        if(nuevoUsuario == null){

            nuevoUsuario = new NuevoUsuario();

            nuevoUsuario.setNombreUsuario(txt_nombreNuevoUsuario.getText());
            nuevoUsuario.setApellidosPaternosUsuario(txt_apellidoPaternoNuevoUsuario.getText());
            nuevoUsuario.setApellidosMaternosUsuario(txt_apellidoMaternoNuevoUsuario.getText());
            nuevoUsuario.setEdadUsuario(txt_edadNuevoUsuario.getText());
            nuevoUsuario.setCelularUsuario(txt_celularNuevoUsuario.getText());
            nuevoUsuario.setCorreoUsuario(txt_correoNuevoUsuario.getText());
            nuevoUsuario.setDniUsuario(txt_nroDniNuevoUsuario.getText());
            nuevoUsuario.setDireccionUsuario(txt_direccionNuevoUsuario.getText());


            NuevoUsuarioDao.crearNuevoU(nuevoUsuario);

        }else {
            nuevoUsuario.setNombreUsuario(txt_nombreNuevoUsuario.getText());
            nuevoUsuario.setApellidosPaternosUsuario(txt_apellidoPaternoNuevoUsuario.getText());
            nuevoUsuario.setApellidosMaternosUsuario(txt_apellidoMaternoNuevoUsuario.getText());
            nuevoUsuario.setEdadUsuario(txt_edadNuevoUsuario.getText());
            nuevoUsuario.setCelularUsuario(txt_celularNuevoUsuario.getText());
            nuevoUsuario.setCorreoUsuario(txt_correoNuevoUsuario.getText());
            nuevoUsuario.setDniUsuario(txt_nroDniNuevoUsuario.getText());
            nuevoUsuario.setDireccionUsuario(txt_direccionNuevoUsuario.getText());

            NuevoUsuarioDao.editarNuevoU(nuevoUsuario);
        }
        btn_cancelarNuevoUsuario(actionEvent);
    }

    public void btn_cancelarNuevoUsuario(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    //crear metodo setter con alt+ins de tipo cliente

    public void setNuevoUsuario(NuevoUsuario nuevoUsuario) {
        this.nuevoUsuario = nuevoUsuario;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDNuevoUsuario.setText(String.valueOf(nuevoUsuario.getIdUsuario()));
        txt_nombreNuevoUsuario.setText(nuevoUsuario.getNombreUsuario());
        txt_apellidoPaternoNuevoUsuario.setText(nuevoUsuario.getApellidosPaternosUsuario());
        txt_apellidoMaternoNuevoUsuario.setText(nuevoUsuario.getApellidosMaternosUsuario());
        txt_edadNuevoUsuario.setText(nuevoUsuario.getEdadUsuario());
        txt_celularNuevoUsuario.setText(nuevoUsuario.getCelularUsuario());
        txt_correoNuevoUsuario.setText(nuevoUsuario.getCorreoUsuario());
        txt_nroDniNuevoUsuario.setText(nuevoUsuario.getDniUsuario());
        txt_direccionNuevoUsuario.setText(nuevoUsuario.getDireccionUsuario());

    }
}
