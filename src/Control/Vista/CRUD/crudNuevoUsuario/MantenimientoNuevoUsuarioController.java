package Control.Vista.CRUD.crudNuevoUsuario;

import Control.Dao.ClienteDao;
import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;

import Modelo.Cliente;
import Modelo.NuevoUsuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoNuevoUsuarioController implements Initializable {

    public Button btn_nuevoUsuarioEditar;
    public Button btn_nuevoUsuarioEliminar;
    
    /*campos de la tabla*/
    public TableColumn<NuevoUsuario, Long> columNuevoUsuarioId;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioNombre;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioApellidosP;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioApellidosM;
    public TableColumn<NuevoUsuario, Integer> columNuevoUsuarioEdad;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioDNI;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioCelular;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioCorreo;
    public TableColumn<NuevoUsuario, String> columNuevoUsuarioDireccion;

    /*campo de la tableView para mostrar datos*/
    public TableView<NuevoUsuario> tblNuevoUsuario;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos de los setter del la clase NuevoUsuario
        columNuevoUsuarioId.setCellValueFactory(new PropertyValueFactory<>("idUsuario"));
        columNuevoUsuarioNombre.setCellValueFactory(new PropertyValueFactory<>("nombreUsuario"));
        columNuevoUsuarioApellidosP.setCellValueFactory(new PropertyValueFactory<>("apellidosPaternosUsuario"));
        columNuevoUsuarioApellidosM.setCellValueFactory(new PropertyValueFactory<>("apellidosMaternosUsuario"));
        columNuevoUsuarioEdad.setCellValueFactory(new PropertyValueFactory<>("edadUsuario"));
        columNuevoUsuarioDNI.setCellValueFactory(new PropertyValueFactory<>("dniUsuario"));
        columNuevoUsuarioCelular.setCellValueFactory(new PropertyValueFactory<>("celularUsuario"));
        columNuevoUsuarioCorreo.setCellValueFactory(new PropertyValueFactory<>("correoUsuario"));
        columNuevoUsuarioDireccion.setCellValueFactory(new PropertyValueFactory<>("direccionUsuario"));


        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_nuevoUsuarioEditar.disableProperty().bind(tblNuevoUsuario.getSelectionModel().selectedItemProperty().isNull());

        btn_nuevoUsuarioEliminar.disableProperty().bind(tblNuevoUsuario.getSelectionModel().selectedItemProperty().isNull());

    }


    /* Metodo para hacer un llamado de varios datos como clienteDao*/
    private void cargarDatos() {

        // Limpiamos la lista para que no se agreguen nuevos datos
        tblNuevoUsuario.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblNuevoUsuario.getItems().addAll(NuevoUsuarioDao.listar());

    }

    public void nuevoNuevoUsuario(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/nuevoNuevoUsuarioCrud.fxml");
        Stage stage = (Stage)datos.get("escenario");
        // Indicamos que se muestre la scena y espere
        stage.showAndWait();
        cargarDatos();
    }

    public Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoNuevoUsuarioDetalleController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    public void editarNuevoUsuario(ActionEvent actionEvent) {

        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/nuevoNuevoUsuarioCrud.fxml");

        MantenimientoNuevoUsuarioDetalleController controlador = (MantenimientoNuevoUsuarioDetalleController) datos.get("controlador");
        controlador.setNuevoUsuario(tblNuevoUsuario.getSelectionModel().getSelectedItem());


        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();

    }

    public void eliminarNuevoUsuario(ActionEvent actionEvent) {

        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Nuevo Usuario");
        dialog.setHeaderText("eliminar Nuevo Usuario");
        dialog.setContentText("Este usuario se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            NuevoUsuarioDao.eliminar(tblNuevoUsuario.getSelectionModel().getSelectedItem());
        }
        cargarDatos();

    }
}
