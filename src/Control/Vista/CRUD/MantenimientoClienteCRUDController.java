package Control.Vista.CRUD;

import Control.Dao.ClienteDao;
import Modelo.Cliente;
import Modelo.Usuario;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoClienteCRUDController implements Initializable {

    // Creamos una variable FXML de tipo Button, que hace referencia al Button del formulario
    public Button btn_clienteEditar;

    // Creamos una variable FXML de tipo Button, que hace referencia al Button del formulario
    public Button btn_clienteEliminar;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, Long> columIdCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columNombreCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columApellidosPCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columApellidosMCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columTipoDocCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columNroDocCliente;

    // Creamos una variable FXML de tipo TableColum, que hace referencia al TableColum del formulario
    public TableColumn<Cliente, String> columCelularCliente;


    // Creamos una variable FXML de tipo TableView, que hace referencia al TableView del formulario
    public TableView <Cliente>tblNuevoCliente;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos
        columIdCliente.setCellValueFactory(new PropertyValueFactory<>("idCliente"));
        columNombreCliente.setCellValueFactory(new PropertyValueFactory<>("nombreCliente"));
        columApellidosPCliente.setCellValueFactory(new PropertyValueFactory<>("apellidoPaternoCliente"));
        columApellidosMCliente.setCellValueFactory(new PropertyValueFactory<>("apellidoMaternoCliente"));
        columTipoDocCliente.setCellValueFactory(new PropertyValueFactory<>("tipoDocumento"));
        columNroDocCliente.setCellValueFactory(new PropertyValueFactory<>("nroDocumento"));
        columCelularCliente.setCellValueFactory(new PropertyValueFactory<>("nroCelular"));

        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_clienteEditar.disableProperty().bind(tblNuevoCliente.getSelectionModel().selectedItemProperty().isNull());
        //btn_clienteEditar.disabledProperty().bind(tblNuevoCliente.getSelectionModel().selectedItemProperty().isNull());
        //el metodo "bind()" me bota error :c
        //psdt a mi no me vota error >:v

        btn_clienteEliminar.disableProperty().bind(tblNuevoCliente.getSelectionModel().selectedItemProperty().isNull());




    }

    /* Metodo para hacer un llamado de varios datos como clienteDao*/
    private void cargarDatos() {

        // Limpiamos la lista para que no se agreguen nuevos datos
        tblNuevoCliente.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblNuevoCliente.getItems().addAll(ClienteDao.listar());

    }

    // Metodo para crear un nuevo cliente
    public void nuevoCliente(ActionEvent actionEvent) {


        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/clienteCrearCRUD.fxml");

        Stage stage = (Stage)datos.get("escenario");

        // Indicamos que se muestre la scena y espere
        stage.showAndWait();


/*

        //usamos el static de ClienteDao del metodo "crear(Cliente  nuevoCliente)"
        Cliente  nuevoCliente = new Cliente();
        nuevoCliente.setNombreCliente("Rodrigo");
        nuevoCliente.setApellidoMaternoCliente("Aguirre");


        ClienteDao.crear(nuevoCliente);
*/


        // Mostramos los datos
        cargarDatos();
    }

    //public Map escenario(String path){
    private Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoClienteCrearCRUDdetalleController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    // Metodo para poder editar un nuevo cliente
    public void editarCliente(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/clienteCrearCRUD.fxml");

        MantenimientoClienteCrearCRUDdetalleController controlador = (MantenimientoClienteCrearCRUDdetalleController) datos.get("controlador");
        controlador.setCliente(tblNuevoCliente.getSelectionModel().getSelectedItem());

        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();

    }

    // Metodo para poder eliminar un nuevo cliente
    public void eliminarCliente(ActionEvent actionEvent) {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Cliente");
        dialog.setHeaderText("eliminar cliente");
        dialog.setContentText("Este usuario se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            ClienteDao.eliminar(tblNuevoCliente.getSelectionModel().getSelectedItem());
        }
        cargarDatos();

    }

}
