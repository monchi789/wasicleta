package Control.Vista.CRUD.crudBicicleta;

import Control.Dao.DaoAlquiler.AlquilerDao;
import Control.Dao.DaoBicicleta.BicicletaDao;
import Control.Dao.DaoUsuario.UsuarioDao;
import Control.Dao.daoTipoBicicleta.TipoBicicletaDao;
import Modelo.Alquiler;
import Modelo.Bicicleta;
import Modelo.NuevoUsuario;
import Modelo.TipoBicicleta;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

public class MantenimientoBicicletaDetalleCRUDController implements Initializable {

    //generar codigo automaticamente
    public String codigoBicicleta = UUID.randomUUID().toString().toUpperCase(Locale.ROOT);

    //cajas de textos para llenar datos
    public TextField txt_IDbicicleta;
    public TextField txt_precioBicicleta;
    public TextField txt_codigoBicicleta;
    public TextField txt_estadoBicicleta;
    //Id de botones para trabajar
    public Button btn_aceptaBicicleta;
    public Button btn_cancelarBicicleta;
    //combo box
    public ComboBox<TipoBicicleta> cmb_tipoBicycleModelo;

    // Creamos una variable de tipo Bicicleta
    private Bicicleta bicicleta;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        txt_IDbicicleta.setDisable(true);
        txt_codigoBicicleta.setDisable(true);

        //btn_aceptarCliente.disableProperty().bind(txt_celularCliente.textProperty().length().lessThan(9));

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_aceptaBicicleta.disableProperty().bind(
                txt_precioBicicleta.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_estadoBicicleta.textProperty().length().lessThanOrEqualTo(0).or(
                                        cmb_tipoBicycleModelo.valueProperty().isNull()
                                )
                        )
                );

        cargarDatos();
        cmb_tipoBicycleModelo.setConverter(new StringConverter<TipoBicicleta>() {
            @Override
            public String toString(TipoBicicleta tipoBicicleta) {
                //hacer excepcion para poder llamar al dato
                if (tipoBicicleta == null)
                    return null;
                return tipoBicicleta.getModeloBicicleta();
            }

            @Override
            public TipoBicicleta fromString(String s) {
                return null;
            }
        });
    }

    private void cargarDatos() {
        cmb_tipoBicycleModelo.getItems().addAll(TipoBicicletaDao.listar());
    }

    public void aceptarBicicleta(ActionEvent actionEvent) {
        if(bicicleta == null){


            bicicleta = new Bicicleta();
            bicicleta.setCodigoBicicleta(codigoBicicleta);
            bicicleta.setPrecioBicicleta(txt_precioBicicleta.getText());
            bicicleta.setEstadoBicicleta(txt_estadoBicicleta.getText());
            bicicleta.setTieneBicicleta(cmb_tipoBicycleModelo.getValue());

            BicicletaDao.crear(bicicleta);

        }else {
            bicicleta.setCodigoBicicleta(codigoBicicleta);
            bicicleta.setPrecioBicicleta(txt_precioBicicleta.getText());
            bicicleta.setEstadoBicicleta(txt_estadoBicicleta.getText());

            bicicleta.setTieneBicicleta(cmb_tipoBicycleModelo.getValue());

            BicicletaDao.crear(bicicleta);
        }
        cancelarBicicleta(actionEvent);
    }

    public void cancelarBicicleta(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void setBicicleta(Bicicleta nuevoBicicleta) {
        this.bicicleta = nuevoBicicleta;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDbicicleta.setText(String.valueOf(bicicleta.getIdBicicleta()));
        txt_precioBicicleta.setText(bicicleta.getPrecioBicicleta());
        txt_codigoBicicleta.setText(bicicleta.getCodigoBicicleta());
        txt_estadoBicicleta.setText(bicicleta.getEstadoBicicleta());

        //cmb_nuevoUsuario.setConverter();
        cmb_tipoBicycleModelo.getSelectionModel().select(bicicleta.getTieneBicicleta());
    }

}
