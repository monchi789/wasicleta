package Control.Vista.CRUD.crudBicicleta;

import Control.Dao.DaoBicicleta.BicicletaDao;
import Modelo.Bicicleta;
import Modelo.Usuario;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoBicicletaCRUDController implements Initializable {
    //id de botones
    public Button btn_bicicletaEditar;
    public Button btn_bicicletaEliminar;
    //tabla de campos
    public TableView<Bicicleta> tblBicicleta;
    //campos
    public TableColumn<Bicicleta, Long> columIdBicicleta;
    public TableColumn<Bicicleta, DecimalFormat> columPrecioBicicleta;
    public TableColumn<Bicicleta, String> columCodigo;
    public TableColumn<Bicicleta, String> columEstadoBici;
    //dato foraneo
    public TableColumn<Bicicleta, String> columModelo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos de los setter del la clase NuevoUsuario setters
        columIdBicicleta.setCellValueFactory(new PropertyValueFactory<>("idBicicleta"));
        columPrecioBicicleta.setCellValueFactory(new PropertyValueFactory<>("precioBicicleta"));
        columCodigo.setCellValueFactory(new PropertyValueFactory<>("codigoBicicleta"));
        columEstadoBici.setCellValueFactory(new PropertyValueFactory<>("estadoBicicleta"));

        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_bicicletaEditar.disableProperty().bind(tblBicicleta.getSelectionModel().selectedItemProperty().isNull());

        btn_bicicletaEliminar.disableProperty().bind(tblBicicleta.getSelectionModel().selectedItemProperty().isNull());

        columModelo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bicicleta, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Bicicleta, String> bicicletaStringCellDataFeatures) {
                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        bicicletaStringCellDataFeatures.getValue().getTieneBicicleta().getModeloBicicleta());
                return salida;
            }
        });




    }


    /* Metodo para hacer un llamado de varios datos como clienteDao*/
    private void cargarDatos() {
        // Limpiamos la lista para que no se agreguen nuevos datos
        tblBicicleta.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblBicicleta.getItems().addAll(BicicletaDao.listar());
    }


    public void nuevoBicicleta(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/bicicletaCRUD.fxml");
        Stage stage = (Stage)datos.get("escenario");
        // Indicamos que se muestre la scena y espere
        stage.showAndWait();
        cargarDatos();
    }

    public Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoBicicletaDetalleCRUDController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    public void editarBicicleta(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/bicicletaCRUD.fxml");

        MantenimientoBicicletaDetalleCRUDController controlador = (MantenimientoBicicletaDetalleCRUDController) datos.get("controlador");
        controlador.setBicicleta(tblBicicleta.getSelectionModel().getSelectedItem());
        ((Stage)datos.get("escenario")).showAndWait();
        // Mostramos los datos
        cargarDatos();
    }

    public void eliminarBicicleta(ActionEvent actionEvent) {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Nuevo Usuario");
        dialog.setHeaderText("eliminar Nuevo Usuario");
        dialog.setContentText("Este usuario se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            BicicletaDao.eliminarBici(tblBicicleta.getSelectionModel().getSelectedItem());
        }
        cargarDatos();
    }


}
