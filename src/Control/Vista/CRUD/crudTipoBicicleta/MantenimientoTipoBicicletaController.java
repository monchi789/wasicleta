package Control.Vista.CRUD.crudTipoBicicleta;

import Control.Dao.ClienteDao;
import Control.Dao.daoTipoBicicleta.TipoBicicletaDao;
import Control.Vista.CRUD.MantenimientoClienteCrearCRUDdetalleController;
import Modelo.TipoBicicleta;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoTipoBicicletaController implements Initializable {

    public Button btn_tipoBiciEditar;
    public Button btn_tipoBiciEliminar;

    /*campo de la tableView para mostrar datos*/
    public TableView<TipoBicicleta> tblTipoBicicleta;

    /*campos de la tabla*/
    public TableColumn<TipoBicicleta, Long> columIdTipoBicicleta;
    public TableColumn<TipoBicicleta, String> columModeloTipoBicicleta;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos usar nombres del campo de las tablas en getters
        columIdTipoBicicleta.setCellValueFactory(new PropertyValueFactory<>("idTipoBIcicleta"));
        columModeloTipoBicicleta.setCellValueFactory(new PropertyValueFactory<>("modeloBicicleta"));


        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_tipoBiciEditar.disableProperty().bind(tblTipoBicicleta.getSelectionModel().selectedItemProperty().isNull());
        btn_tipoBiciEliminar.disableProperty().bind(tblTipoBicicleta.getSelectionModel().selectedItemProperty().isNull());

    }

    /* Metodo para hacer un llamado de varios datos como clienteDao*/
    private void cargarDatos() {

        // Limpiamos la lista para que no se agreguen nuevos datos
        tblTipoBicicleta.getItems().clear();

        // Le pasamos al tableView los usuarios
        //tblTipoBicicleta.getItems().addAll(TipoBicicletaDao.listar());
        tblTipoBicicleta.getItems().addAll(TipoBicicletaDao.listar());

    }


    public void nuevoTipoBicicleta(ActionEvent actionEvent) {


        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/tipoBicicletaNuevoCRUD.fxml");

        Stage stage = (Stage)datos.get("escenario");

        // Indicamos que se muestre la scena y espere
        stage.showAndWait();

        // Mostramos los datos
        cargarDatos();
    }

    //public Map escenario(String path){
    public Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoTipoBicicletaCrearCRUDdetalleController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }


    public void editarTipoBicicleta(ActionEvent actionEvent) {

        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/tipoBicicletaNuevoCRUD.fxml");

        //registro seleccionado se encuentra en tableView
        MantenimientoTipoBicicletaCrearCRUDdetalleController controlador = (MantenimientoTipoBicicletaCrearCRUDdetalleController) datos.get("controlador");
        controlador.setTipoBicicleta(tblTipoBicicleta.getSelectionModel().getSelectedItem());


        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();


    }

    public void elimiarTipoBicicleta(ActionEvent actionEvent) {

        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Tipo de Bicicleta");
        dialog.setHeaderText("eliminar Tipo de Bicicleta");
        dialog.setContentText("Este usuario se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            TipoBicicletaDao.eliminar(tblTipoBicicleta.getSelectionModel().getSelectedItem());
        }
        cargarDatos();
    }
}
