package Control.Vista.CRUD.crudTipoBicicleta;

import Control.Dao.ClienteDao;
import Control.Dao.daoTipoBicicleta.TipoBicicletaDao;
import Modelo.Cliente;
import Modelo.TipoBicicleta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

public class MantenimientoTipoBicicletaCrearCRUDdetalleController implements Initializable {



    public Button btn_aceptarTipoBicicleta;

    public TextField txt_IDtipoBicicleta;
    public TextField txt_modeloTipoBicicleta;
    //comboBox
    public ComboBox<String> cmb_tipoBicicleta;
    ObservableList<String> tiposBicicleta = FXCollections.observableArrayList("BMX","Montañera","Ciudad","Ruta","Profesional urbano");

    // Creamos una variable de tipo TipoBicicelta
    private TipoBicicleta tipoBicicleta;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        txt_IDtipoBicicleta.setDisable(true);

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        //btn_aceptarTipoBicicleta.disableProperty().bind(cmb_tipoBicicleta.getValue()..lessThan(0));
        cargarDatos();

    }

    private void cargarDatos() {

        cmb_tipoBicicleta.setItems(tiposBicicleta);
    }

    public void aceptarTipoBicicleta(ActionEvent actionEvent) {

        if(tipoBicicleta == null){

            tipoBicicleta = new TipoBicicleta();

            tipoBicicleta.setModeloBicicleta(cmb_tipoBicicleta.getValue());

            TipoBicicletaDao.crearTipoBici(tipoBicicleta);

        }else {
           tipoBicicleta.setModeloBicicleta(cmb_tipoBicicleta.getValue());

            TipoBicicletaDao.editarTipoBici(tipoBicicleta);
        }

        btn_cancelarTipoBicicleta(actionEvent);

    }

    public void btn_cancelarTipoBicicleta(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void setTipoBicicleta(TipoBicicleta tipoBicicleta) {
        this.tipoBicicleta = tipoBicicleta;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDtipoBicicleta.setText(String.valueOf(tipoBicicleta.getIdTipoBIcicleta()));
        txt_modeloTipoBicicleta.setText(tipoBicicleta.getModeloBicicleta());
    }

}
