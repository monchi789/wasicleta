package Control.Vista.CRUD.crudAlquiler;

import Control.Dao.ClienteDao;
import Control.Dao.DaoAlquiler.AlquilerDao;
import Control.Dao.DaoUsuario.UsuarioDao;

import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;
import Modelo.*;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

public class MantenimientoAlquilerDetalleCRUDController implements Initializable {

    //generar codigo automaticamente
    public String codigo = UUID.randomUUID().toString().toUpperCase(Locale.ROOT);

    public TextField txt_IDalquiler;
    public TextField txt_fechaAlquiler;
    public TextField txt_codigoAlquiler;
    public TextField txt_montoAlquiler;

    public Button btn_aceptarAlquiler;
    public Button btn_cancelarAlquiler;

    //relaciones combo box one to many
    public ComboBox<Usuario> cmb_usuario;
    public ComboBox<Cliente> cmb_cliente;

    //label "TITULO"
    public Label lbl_alquiler;

    // Creamos una variable de tipo Cliente
    private Alquiler alquiler;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        txt_IDalquiler.setDisable(true);

        txt_codigoAlquiler.setDisable(true);

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_aceptarAlquiler.disableProperty().bind(
                txt_montoAlquiler.textProperty().length().lessThanOrEqualTo(0));

        //combo box
        cargarDatos();
        cmb_cliente.setConverter(new StringConverter<Cliente>() {
            @Override
            public String toString(Cliente cliente) {
                //hacer excepcion para poder llamar al dato
                if (cliente == null)
                    return null;
                return cliente.getNombreCliente();
            }

            @Override
            public Cliente fromString(String s) {
                return null;
            }
        });




        cmb_usuario.setConverter(new StringConverter<Usuario>() {
            @Override
            public String toString(Usuario usuario) {
                //hacer excepcion para poder llamar al dato
                if (usuario == null)
                    return null;
                return usuario.getUsuario();
            }

            @Override
            public Usuario fromString(String s) {
                return null;
            }
        });


    }
    //metodo para el combox
    private void cargarDatos() {

        cmb_usuario.getItems().addAll(UsuarioDao.listar());
        cmb_cliente.getItems().addAll(ClienteDao.listar());
    }

    public void aceptarAlquiler(ActionEvent actionEvent) {
        if(alquiler == null){

            alquiler = new Alquiler();

            alquiler.setFechaAlquiler(txt_fechaAlquiler.getText());
            alquiler.setCodigoAlquiler(codigo);
            alquiler.setMontoALquiler(txt_montoAlquiler.getText());

            //combo box
            //alquiler.setPerteneceHistorialAlquiler(cmb_historialAlquiler.getValue());
            alquiler.setPerteneceCliente(cmb_cliente.getValue());
            alquiler.setPerteneceUsuario(cmb_usuario.getValue());

            AlquilerDao.crear(alquiler);

        }else {
            alquiler.setFechaAlquiler(txt_fechaAlquiler.getText());
            alquiler.setCodigoAlquiler(codigo);
            alquiler.setMontoALquiler(txt_montoAlquiler.getText());
            //combo box
            //alquiler.setPerteneceHistorialAlquiler(cmb_historialAlquiler.getValue());
            alquiler.setPerteneceCliente(cmb_cliente.getValue());
            alquiler.setPerteneceUsuario(cmb_usuario.getValue());

            AlquilerDao.editar(alquiler);
        }
        scancelarAlquiler(actionEvent);
    }


    public void scancelarAlquiler(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }
    public void setAlquiler(Alquiler nuevoAlquiler) {
        this.alquiler = nuevoAlquiler;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDalquiler.setText(String.valueOf(alquiler.getIdAlquiler()));
        txt_fechaAlquiler.setText(alquiler.getFechaAlquiler());
        txt_codigoAlquiler.setText(alquiler.getCodigoAlquiler());
        txt_codigoAlquiler.setText(alquiler.getMontoALquiler());
        //mostrar datos de combo box
        lbl_alquiler.setText("Editar Alquiler");
    }
}
