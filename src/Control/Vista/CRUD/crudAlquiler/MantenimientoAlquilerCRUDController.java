package Control.Vista.CRUD.crudAlquiler;

import Control.Dao.DaoAlquiler.AlquilerDao;
import Modelo.Alquiler;
import Modelo.Usuario;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.util.*;

public class MantenimientoAlquilerCRUDController implements Initializable {
    //botones id
    public Button btn_alquilarEditar;
    public Button btn_alquilarEliminar;
    //tabla para los campos
    public TableView<Alquiler> tblAlquiler;
    //campos
    public TableColumn<Alquiler, Long> columIdAlquiler;
    public TableColumn<Alquiler, DateFormat> columFechaAlquiler;
    public TableColumn<Alquiler, String> columCodigoAlquiler;
    //forein key
    public TableColumn<Alquiler, String> columUsuario;
    public TableColumn<Alquiler, String> columCliente;
    public TableColumn<Alquiler, String> columMontoAlquiler;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos de los setter del la clase NuevoUsuario setters
        columIdAlquiler.setCellValueFactory(new PropertyValueFactory<>("idAlquiler"));
        columFechaAlquiler.setCellValueFactory(new PropertyValueFactory<>("fechaAlquiler"));
        columCodigoAlquiler.setCellValueFactory(new PropertyValueFactory<>("codigoAlquiler"));
        columMontoAlquiler.setCellValueFactory(new PropertyValueFactory<>("montoALquiler"));


        columUsuario.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Alquiler, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Alquiler, String> alquilerStringCellDataFeatures) {
                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        alquilerStringCellDataFeatures.getValue().getPerteneceUsuario().getUsuario()
                );
                return salida;
            }
        });

        columCliente.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Alquiler, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Alquiler, String> alquilerStringCellDataFeatures) {

                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        alquilerStringCellDataFeatures.getValue().getPerteneceCliente().getNombreCliente()
                );
                return salida;
            }
        });

        /*columHistorialAlquiler.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Alquiler, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Alquiler, String> alquilerStringCellDataFeatures) {
                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        alquilerStringCellDataFeatures.getValue().getPerteneceHistorialAlquiler().getEstado());
                return salida;
            }
        });*/




        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_alquilarEditar.disableProperty().bind(tblAlquiler.getSelectionModel().selectedItemProperty().isNull());

        btn_alquilarEliminar.disableProperty().bind(tblAlquiler.getSelectionModel().selectedItemProperty().isNull());

    }

    /* Metodo para hacer un llamado de varios datos como clienteDao*/
    private void cargarDatos() {

        // Limpiamos la lista para que no se agreguen nuevos datos
        tblAlquiler.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblAlquiler.getItems().addAll(AlquilerDao.listar());

    }

    public void nuevoAlquiler(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/alquilerCRUD.fxml");
        Stage stage = (Stage)datos.get("escenario");
        // Indicamos que se muestre la scena y espere
        stage.showAndWait();
        cargarDatos();
    }

    public Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoAlquilerDetalleCRUDController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    public void editarAlquiler(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/alquilerCRUD.fxml");

        MantenimientoAlquilerDetalleCRUDController controlador = (MantenimientoAlquilerDetalleCRUDController) datos.get("controlador");
        controlador.setAlquiler(tblAlquiler.getSelectionModel().getSelectedItem());


        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();
    }

    public void eliminarAlquiler(ActionEvent actionEvent) {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Nuevo Usuario");
        dialog.setHeaderText("eliminar Alquiler");
        dialog.setContentText("Este alquiler se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            AlquilerDao.eliminar(tblAlquiler.getSelectionModel().getSelectedItem());
        }
        cargarDatos();
    }


}
