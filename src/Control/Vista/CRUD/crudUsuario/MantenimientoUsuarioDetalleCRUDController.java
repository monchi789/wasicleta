package Control.Vista.CRUD.crudUsuario;

import Control.Dao.ClienteDao;
import Control.Dao.DaoUsuario.UsuarioDao;
import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;
import Control.encripta.Encripta;
import Modelo.Cliente;
import Modelo.NuevoUsuario;
import Modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class MantenimientoUsuarioDetalleCRUDController implements Initializable {
    public TextField txt_IDusuario;
    public TextField txt_usuarioUsuario;
    public TextField txt_passwordUsuario;
    public Button btn_aceptarUsuario;
    public Button btn_cancelarUsuario;
    public ComboBox<NuevoUsuario> cmb_nuevoUsuario;
    public Label lbl_usuario;

    // Creamos una variable de tipo Cliente
    private Usuario usuario;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        txt_IDusuario.setDisable(true);

        //btn_aceptarCliente.disableProperty().bind(txt_celularCliente.textProperty().length().lessThan(9));

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_aceptarUsuario.disableProperty().bind(
                txt_usuarioUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_passwordUsuario.textProperty().length().lessThanOrEqualTo(0).or(
                                cmb_nuevoUsuario.valueProperty().isNull()
                        )
                ));



        //datos del combo box
        cargarDatos();
        cmb_nuevoUsuario.setConverter(new StringConverter<NuevoUsuario>() {
            @Override
            public String toString(NuevoUsuario nuevoUsuario) {
                //hacer excepcion para poder llamar al dato
                if (nuevoUsuario == null)
                    return null;
                return nuevoUsuario.getDniUsuario();
            }

            @Override
            public NuevoUsuario fromString(String s) {
                return null;
            }
        });

    }
    //metodo cargar datos combo box
    private void cargarDatos() {
        cmb_nuevoUsuario.getItems().addAll(NuevoUsuarioDao.listar());
    }

    public void aceptarUsuario(ActionEvent actionEvent) {
        if(usuario == null){

            usuario = new Usuario();

            usuario.setUsuario(txt_usuarioUsuario.getText());
            usuario.setConstrasenaUsuario(Encripta.encripta(txt_passwordUsuario.getText()));

            //combo box
            usuario.setPerteneceNuevoUsuario(cmb_nuevoUsuario.getValue());

            UsuarioDao.crear(usuario);

        }else {
            usuario.setUsuario(txt_usuarioUsuario.getText());
            usuario.setConstrasenaUsuario(Encripta.encripta(txt_passwordUsuario.getText()));

            //combo box
            usuario.setPerteneceNuevoUsuario(cmb_nuevoUsuario.getValue());

            UsuarioDao.editar(usuario);
        }

        btn_cancelarUsuario(actionEvent);
    }

    public void btn_cancelarUsuario(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    //crear metodo setter con alt+ins de tipo cliente
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDusuario.setText(String.valueOf(usuario.getIdUsuario()));
        txt_usuarioUsuario.setText(usuario.getUsuario());
        txt_passwordUsuario.setText(usuario.getConstrasenaUsuario());
        //mostrar datos de combo box
        lbl_usuario.setText("Editar Usuario");
        //cmb_nuevoUsuario.setConverter();
        cmb_nuevoUsuario.getSelectionModel().select(usuario.getPerteneceNuevoUsuario());


    }
}
