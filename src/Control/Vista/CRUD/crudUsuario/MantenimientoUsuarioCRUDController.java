package Control.Vista.CRUD.crudUsuario;

import Control.Dao.ClienteDao;
import Control.Dao.DaoUsuario.UsuarioDao;
import Control.Dao.daoNuevoUsuario.NuevoUsuarioDao;
import Control.Vista.CRUD.MantenimientoClienteCrearCRUDdetalleController;
import Modelo.NuevoUsuario;
import Modelo.Usuario;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoUsuarioCRUDController implements Initializable {


    public Button btn_usuarioEditar;
    public Button btn_usuarioEliminar;

    //tabla
    public TableView<Usuario> tblUsuario;

    //Campos de la tabla
    public TableColumn<Usuario, Long> columIdUsuario;
    public TableColumn<Usuario, String> columUsuario;
    public TableColumn<Usuario, String>  columPasswordUsuario;
    public TableColumn<Usuario, String> columNuevoUsuario;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos
        columIdUsuario.setCellValueFactory(new PropertyValueFactory<>("idUsuario"));
        columUsuario.setCellValueFactory(new PropertyValueFactory<>("Usuario"));
        columPasswordUsuario.setCellValueFactory(new PropertyValueFactory<>("constrasenaUsuario"));


        columNuevoUsuario.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Usuario, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Usuario, String> usuarioStringCellDataFeatures) {
                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        usuarioStringCellDataFeatures.getValue().getPerteneceNuevoUsuario().getDniUsuario());
                return salida;
            }
        });

        //mostrar datos del forein key creando una clase implementado la el callback y sin nombre
        /*columNuevoUsuario.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Usuario, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Usuario, String> usuarioStringCellDataFeatures) {

                ReadOnlyObjectWrapper salida  = new ReadOnlyObjectWrapper(

                        usuarioStringCellDataFeatures.getValue().getPerteneceNuevoUsuario().getDniUsuario()
                );

                return salida;
            }
        });
*/

        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_usuarioEditar.disableProperty().bind(tblUsuario.getSelectionModel().selectedItemProperty().isNull());
        //btn_clienteEditar.disabledProperty().bind(tblNuevoCliente.getSelectionModel().selectedItemProperty().isNull());
        //el metodo "bind()" me bota error :c
        //psdt a mi no me vota error >:v

        btn_usuarioEliminar.disableProperty().bind(tblUsuario.getSelectionModel().selectedItemProperty().isNull());

    }

    private void cargarDatos() {
        // Limpiamos la lista para que no se agreguen nuevos datos
        tblUsuario.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblUsuario.getItems().addAll(UsuarioDao.listar());
    }

    public void nuevoUsuario(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/usuarioCrearCRUD.fxml");

        Stage stage = (Stage)datos.get("escenario");

        // Indicamos que se muestre la scena y espere
        stage.showAndWait();


/*

        //usamos el static de ClienteDao del metodo "crear(Cliente  nuevoCliente)"
        Cliente  nuevoCliente = new Cliente();
        nuevoCliente.setNombreCliente("Rodrigo");
        nuevoCliente.setApellidoMaternoCliente("Aguirre");


        ClienteDao.crear(nuevoCliente);
*/


        // Mostramos los datos
        cargarDatos();
    }

    private Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoUsuarioDetalleCRUDController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    public void editarUsuario(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/usuarioCrearCRUD.fxml");

        MantenimientoUsuarioDetalleCRUDController controlador = (MantenimientoUsuarioDetalleCRUDController) datos.get("controlador");
        controlador.setUsuario(tblUsuario.getSelectionModel().getSelectedItem());


        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();
    }

    public void eliminarUsuario(ActionEvent actionEvent) {

        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Nuevo Usuario");
        dialog.setHeaderText("eliminar Nuevo Usuario");
        dialog.setContentText("Este usuario se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            UsuarioDao.eliminar(tblUsuario.getSelectionModel().getSelectedItem());
        }
        cargarDatos();

    }

}
