package Control.Vista.CRUD.crudDetalleDeAlquiler;

import Control.Dao.DaoDetalleDeAlquiler.DetalleAlquilerDao;

import Modelo.Bicicleta;
import Modelo.DetalleDeAlquiler;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

public class MantenimientoDetalleAlquilerCRUDController implements Initializable {

    //id de los botones
    public Button btn_detalleAlquilarEditar;
    public Button btn_detalleAlquilarEliminar;
    //talbla para los campos
    public TableView<DetalleDeAlquiler> tblDetalleAlquiler;
    //campos
    public TableColumn<DetalleDeAlquiler, Long> columIDetalleAlquiler;
    public TableColumn<DetalleDeAlquiler, Double>  columDescuentoDetalleAlquiler;
    public TableColumn<DetalleDeAlquiler, String>  columFechaDetalleAlquiler;
    public TableColumn<DetalleDeAlquiler, Integer>  columCantidadDetalleAlquiler;
    public TableColumn<DetalleDeAlquiler, Double>  columTotalDetalleAlquiler;
    //datos foraneos
    public TableColumn<DetalleDeAlquiler, String> columCodigoAlquiler;
    public TableColumn<DetalleDeAlquiler, String> columCodigoBicicleta;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /* creamos un metodo para llamar varios datos en este caso se llama cargarDatos*/
        cargarDatos();

        // Mostramos los datos de los setter de la clase HistorialALquiler
        columIDetalleAlquiler.setCellValueFactory(new PropertyValueFactory<>("idDetalleAlquiler"));
        columDescuentoDetalleAlquiler.setCellValueFactory(new PropertyValueFactory<>("descuentoDetalleAlquiler"));
        columFechaDetalleAlquiler.setCellValueFactory(new PropertyValueFactory<>("fechaDetalleAlquiler"));
        columCantidadDetalleAlquiler.setCellValueFactory(new PropertyValueFactory<>("cantidadDetalleAlquiler"));
        columTotalDetalleAlquiler.setCellValueFactory(new PropertyValueFactory<>("totalDelAlquiler"));

        //desabilitar el boton editar amenos que seleccionesmos una fila para editar
        btn_detalleAlquilarEditar.disableProperty().bind(tblDetalleAlquiler.getSelectionModel().selectedItemProperty().isNull());
        btn_detalleAlquilarEliminar.disableProperty().bind(tblDetalleAlquiler.getSelectionModel().selectedItemProperty().isNull());

       columCodigoAlquiler.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DetalleDeAlquiler, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<DetalleDeAlquiler, String> detalleDeAlquilerStringCellDataFeatures) {

                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                  detalleDeAlquilerStringCellDataFeatures.getValue().getPerteneceDetalleAlquiler().getCodigoAlquiler());
                return salida;
            }
        });
/*
       columCodigoBicicleta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<DetalleDeAlquiler, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<DetalleDeAlquiler, String> detalleDeAlquilerStringCellDataFeatures) {
                ReadOnlyObjectWrapper salida = new ReadOnlyObjectWrapper(
                        detalleDeAlquilerStringCellDataFeatures.getValue().getRefiereBicicleta().getCodigoBicicleta());                                        );
                return salida;
            }
        });*/
    }

    private void cargarDatos() {
        // Limpiamos la lista para que no se agreguen nuevos datos
        tblDetalleAlquiler.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblDetalleAlquiler.getItems().addAll(DetalleAlquilerDao.listar());
    }

    public void nuevoDetalleAlquiler(ActionEvent actionEvent) {
        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/detalleAlquilerCRUD.fxml");
        Stage stage = (Stage)datos.get("escenario");
        // Indicamos que se muestre la scena y espere
        stage.showAndWait();
        cargarDatos();
    }
    //hacemos un mapeo para llamar a las interfaces fxl para nuevo y editar
    public Map escenario(String path){

        // Creamos un objeto de tipo Stage
        Stage stage = new Stage();

        MantenimientoDetalleAlquilerDetalleCRUDController controlador = null;

        // Try y catch para el  manejo de errores
        try {

            FXMLLoader cargador = new FXMLLoader(getClass().getResource(path));

            // Creamos una variable de tipo root donde pasamos por parametro el path
            Parent root = cargador.load();

            controlador = cargador.getController();

            // Creamos un objeto de tipo scena y pasamos por parametro el root
            Scene scene = new Scene(root);

            // Ponemos la scena en el escenario
            stage.setScene(scene);

            // Indicamos que no debe perder el foco
            stage.initModality(Modality.APPLICATION_MODAL);


        }catch (IOException e){

            System.out.println(e.getMessage());

        }

        /*usamos un map que es parecido a un diccionario en py para poder trabajar con el
        par clave "String" y un valor */
        Map salida = new HashMap();

        salida.put("escenario", stage);
        salida.put("controlador", controlador);

        return salida;

    }

    public void editarDetalleAlquiler(ActionEvent actionEvent) {

        // Cargamos la escena
        Map datos = escenario("/Vista/crud/crudCreateData/detalleAlquilerCRUD.fxml");
        MantenimientoDetalleAlquilerDetalleCRUDController controlador = (MantenimientoDetalleAlquilerDetalleCRUDController) datos.get("controlador");
        controlador.setDetalleAlquiler(tblDetalleAlquiler.getSelectionModel().getSelectedItem());


        ((Stage)datos.get("escenario")).showAndWait();

        // Mostramos los datos
        cargarDatos();

    }

    public void eliminarDetalleAlquiler(ActionEvent actionEvent) {

        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Eliminar Historial de Alquiler");
        dialog.setHeaderText("eliminar Historial de Alquiler");
        dialog.setContentText("Este campo se eliminara permanentemente!");
        Optional<ButtonType> resultado = dialog.showAndWait();
        if (resultado.get() == ButtonType.OK){
            DetalleAlquilerDao.eliminarDetalleA(tblDetalleAlquiler.getSelectionModel().getSelectedItem());
        }
        cargarDatos();


    }
}
