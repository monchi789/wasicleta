package Control.Vista.CRUD.crudDetalleDeAlquiler;

import Control.Dao.DaoAlquiler.AlquilerDao;
import Control.Dao.DaoDetalleDeAlquiler.DetalleAlquilerDao;

import Modelo.*;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class MantenimientoDetalleAlquilerDetalleCRUDController implements Initializable {

    //textos para llenar los datos de los campos
    public TextField txt_cantidadDetalleAlquiler;
    public TextField txt_totalDetalleAlquiler;
    public TextField txt_IDdetalleAlquiler;
    public TextField txt_descuentoDetalleAlquiler;
    public TextField txt_fechaDetalleAlquiler;
    //id botones
    public Button btn_aceptarDetalleAlquiler;
    public Button btn_cancelarDetalleAlquiler;


    // Creamos una variable de tipo Cliente
    private DetalleDeAlquiler detalleDeAlquiler;

    //comobo box
    public ComboBox<Alquiler> cmb_codigoAlquiiler;
    public ComboBox<Bicicleta> cmb_codigoBicicleta;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        txt_IDdetalleAlquiler.setDisable(true);

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_cancelarDetalleAlquiler.disableProperty().bind(
                txt_totalDetalleAlquiler.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_fechaDetalleAlquiler.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_cantidadDetalleAlquiler.textProperty().length().lessThanOrEqualTo(0).or(
                                        cmb_codigoAlquiiler.valueProperty().isNull().or(
                                                cmb_codigoBicicleta.valueProperty().isNull()
                                        )
                                )
                        )
                ));
        cargarDatos();
        cmb_codigoBicicleta.setConverter(new StringConverter<Bicicleta>() {
            @Override
            public String toString(Bicicleta bicicleta) {
                //hacer excepcion para poder llamar al dato
                if (bicicleta == null)
                    return null;
                return bicicleta.getCodigoBicicleta();

            }

            @Override
            public Bicicleta fromString(String s) {
                return null;
            }
        });

        cmb_codigoAlquiiler.setConverter(new StringConverter<Alquiler>() {
            @Override
            public String toString(Alquiler alquiler) {
                //hacer excepcion para poder llamar al dato
                if (alquiler == null)
                    return null;
                return alquiler.getCodigoAlquiler();
            }

            @Override
            public Alquiler fromString(String s) {
                return null;
            }
        });
    }

    private void cargarDatos() {

        cmb_codigoAlquiiler.getItems().addAll(AlquilerDao.listar());
        //cmb_codigoBicicleta.getItems().addAll(BicicletaDao.listar());

    }

    public void aceptarDetalleAlquiler(ActionEvent actionEvent) {
        if(detalleDeAlquiler == null){

            detalleDeAlquiler = new DetalleDeAlquiler();

            detalleDeAlquiler.setCantidadDetalleAlquiler(txt_cantidadDetalleAlquiler.getText());
            detalleDeAlquiler.setTotalDelAlquiler(txt_totalDetalleAlquiler.getText());
            detalleDeAlquiler.setDescuentoDetalleAlquiler(txt_descuentoDetalleAlquiler.getText());
            detalleDeAlquiler.setFechaDetalleAlquiler(txt_fechaDetalleAlquiler.getText());
            //usar datos foraneos
            detalleDeAlquiler.setPerteneceDetalleAlquiler(cmb_codigoAlquiiler.getValue());
            detalleDeAlquiler.setRefiereBicicleta(cmb_codigoBicicleta.getValue());




            DetalleAlquilerDao.crear(detalleDeAlquiler);

        }else {
            detalleDeAlquiler.setCantidadDetalleAlquiler(txt_cantidadDetalleAlquiler.getText());
            detalleDeAlquiler.setTotalDelAlquiler(txt_totalDetalleAlquiler.getText());
            detalleDeAlquiler.setDescuentoDetalleAlquiler(txt_descuentoDetalleAlquiler.getText());
            detalleDeAlquiler.setFechaDetalleAlquiler(txt_fechaDetalleAlquiler.getText());
            //datos foraneos
            detalleDeAlquiler.setPerteneceDetalleAlquiler(cmb_codigoAlquiiler.getValue());
            detalleDeAlquiler.setRefiereBicicleta(cmb_codigoBicicleta.getValue());

            DetalleAlquilerDao.editarDetalle(detalleDeAlquiler);
        }
        cancelarDetalleAlquiler(actionEvent);

    }

    public void cancelarDetalleAlquiler(ActionEvent actionEvent) {
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void setDetalleAlquiler(DetalleDeAlquiler detalleDeAlquiler) {
        this.detalleDeAlquiler = detalleDeAlquiler;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDdetalleAlquiler.setText(String.valueOf(detalleDeAlquiler.getIdDetalleAlquiler()));
        txt_cantidadDetalleAlquiler.setText(detalleDeAlquiler.getCantidadDetalleAlquiler());
        txt_totalDetalleAlquiler.setText(detalleDeAlquiler.getTotalDelAlquiler());
        txt_descuentoDetalleAlquiler.setText(detalleDeAlquiler.getDescuentoDetalleAlquiler());
        txt_fechaDetalleAlquiler.setText(detalleDeAlquiler.getFechaDetalleAlquiler());

        //cmb_nuevoUsuario.setConverter();
        cmb_codigoAlquiiler.getSelectionModel().select(detalleDeAlquiler.getPerteneceDetalleAlquiler());
        cmb_codigoBicicleta.getSelectionModel().select(detalleDeAlquiler.getRefiereBicicleta());
    }
}
