package Control.Vista.CRUD;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PrincipalCRUDController implements Initializable {

    // Creamos una variable FXML de tipo HBox, que hace referencia al HBox del formulario
    public HBox HBoxCRUDPrincipal;

    // Creamos una variable FXML de tipo ToggleGroup, que hace referencia al grupo de ToggleGroup
    public ToggleGroup CRUDPrincipal;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    // Metodo que se llama cuando preisonamos el ToggleButton CRUDNuevoUsuario
    public void CRUDNuevoUsuario(ActionEvent actionEvent) {
        // Llamamos al metodo LoadPage que sirve para cargar una pagina dentro del HBox y pasamos por parametro el
        // nombre del archivo fxml
        LoadPage(actionEvent, "CRUDUsuarioNuevo");

    }

    // Metodo que se llama cuando preisonamos el ToggleButton CRUDNuevoUsuario
    public void CRUDTipoBicicleta(ActionEvent actionEvent) {
        // Llamamos al metodo LoadPage que sirve para cargar una pagina dentro del HBox y pasamos por parametro el
        // nombre del archivo fxml
        LoadPage(actionEvent, "CRUDTipoBicicleta");

    }

    // Metodo que se llama cuando preisonamos el ToggleButton CRUDNuevoUsuario
    public void CRUDInventario(ActionEvent actionEvent) {
        // Llamamos al metodo LoadPage que sirve para cargar una pagina dentro del HBox y pasamos por parametro el
        // nombre del archivo fxml
        //LoadPage(actionEvent, "CRUDInventario");
    }

    // Metodo que se llama cuando preisonamos el ToggleButton CRUDNuevoUsuario
    public void CRUDHistorialDeAlquiler(ActionEvent actionEvent) {
        // Llamamos al metodo LoadPage que sirve para cargar una pagina dentro del HBox y pasamos por parametro el
        // nombre del archivo fxml
        LoadPage(actionEvent, "CRUDHistorialDeAlquiler");

    }

    // Metodo que se llama cuando preisonamos el ToggleButton CRUDNuevoUsuario
    public void CRUDCliente(ActionEvent actionEvent) {
        // Llamamos al metodo LoadPage que sirve para cargar una pagina dentro del HBox y pasamos por parametro el
        // nombre del archivo fxml
        LoadPage(actionEvent, "CRUDCliente");

    }

    // Metodo para poder cerrar la pagina principal
    public void Close(ActionEvent event) {

        // Metodo para poder cerrar el login
        ((Stage)((Node)event.getSource()).getScene().getWindow()).close();

    }

    // Creamos un metodo para poder cargar un "AnchorPane" dentro del "HBox"
    private void LoadPage(ActionEvent event, String page){

        // Consultamos si dentro de los componenentes del HBoxPrincipal esta con algun formulario dentro
        if(HBoxCRUDPrincipal.getChildren().size() > 1){

            // En el caso que sea mayor de 1, se removera el componente que esta en el indice 1
            HBoxCRUDPrincipal.getChildren().remove(1);

        }

        // Al presionar el ToggleButton se cargara el formulario al cual hace referencia
        if(((ToggleButton)event.getSource()).isSelected()){

            try {
                // Creamos una variable de tipo parent donde estara la direccion del archivo FXML
                Parent root  = FXMLLoader.load(getClass().getResource("/Vista/crud/" + page + ".fxml"));


                // Cargamos dentro del "HBox" el root
                HBoxCRUDPrincipal.getChildren().add(root);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private double posX = 0;
    private double posY = 0;

    @FXML
    void Salir(ActionEvent event) {
        try {
            Stage xd = new Stage();
            ((Stage)((Node)event.getSource()).getScene().getWindow()).close();
            Parent root  = FXMLLoader.load(getClass().getResource("/Vista/login.fxml"));
            Scene scene = new Scene(root);
            xd.initStyle(StageStyle.TRANSPARENT);
            scene.setFill(Color.TRANSPARENT);
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    posX = event.getSceneX();
                    posY = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xd.setX(event.getScreenX() - posX);
                    xd.setY(event.getScreenY() - posY);
                }
            });
            xd.setScene(scene);
            xd.show();
        }catch (IOException e){
        }
    }

    public void CRUDUsuario(ActionEvent actionEvent) {
        LoadPage(actionEvent, "CRUDUsuario");
    }

    public void CRUDAlquiler(ActionEvent actionEvent) {
        LoadPage(actionEvent, "CRUDAlquiler");
    }

    public void CRUDdetalleDeAlquiler(ActionEvent actionEvent) {
        LoadPage(actionEvent, "CRUDdetalleALquiler");
    }

    public void CRUDBicicleta(ActionEvent actionEvent) {
        LoadPage(actionEvent, "CRUDbicicleta");
    }
}
