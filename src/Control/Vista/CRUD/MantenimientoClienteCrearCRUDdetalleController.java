package Control.Vista.CRUD;

import Control.Dao.ClienteDao;
import Modelo.Cliente;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;

public class MantenimientoClienteCrearCRUDdetalleController implements Initializable {

    // Creamos una variable de tipo Button
    public Button btn_aceptarCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_IDCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_nombreCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_apellidoPaternoCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_apellidoMaternoCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_tipoDocumentoCliente;
    //public TextField txt_TipoCliente;
    //public ComboBox cmd_TipoCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_nroDocumentoCliente;

    // Creamos una variable de tipo TextField
    public TextField txt_celularCliente;

    public ComboBox<String> cmb_tipoDocumentoCliente;

    //lista para el comboBox
    ObservableList<String> list = FXCollections.observableArrayList("Pasaporte","DNI","Libreta militar","Licencia de conducir");


    // Creamos una variable de tipo Cliente
    private Cliente cliente;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        txt_IDCliente.setDisable(true);

        //btn_aceptarCliente.disableProperty().bind(txt_celularCliente.textProperty().length().lessThan(9));

        //se habilitara el boton si es las letras son mayores a 0
        //el boton esta deshabilitado porque en el texto celular las letras son menores o iguales a 0
        btn_aceptarCliente.disableProperty().bind(
                txt_celularCliente.textProperty().length().lessThanOrEqualTo(0).or(
                        txt_nombreCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                txt_apellidoMaternoCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                        txt_apellidoPaternoCliente.textProperty().length().lessThanOrEqualTo(0).or(
                                                txt_nroDocumentoCliente.textProperty().length().lessThanOrEqualTo(0)
                                        )
                                )
                        )
                ));
        //datos para el combo Box de tipo de Documento
        //combo box
        cargarDatos();
    }

    private void cargarDatos() {
        //cmb_cliente.getItems().addAll(ClienteDao.listar());
        cmb_tipoDocumentoCliente.setItems(list);
    }


    //boton de aceptar y agregar nuevo usuario
    public void aceptarCliente(ActionEvent actionEvent) {

        if(cliente == null){

            cliente = new Cliente();

            cliente.setNombreCliente(txt_nombreCliente.getText());
            cliente.setApellidoPaternoCliente(txt_apellidoPaternoCliente.getText());
            cliente.setApellidoMaternoCliente(txt_apellidoMaternoCliente.getText());
            cliente.setTipoDocumento(cmb_tipoDocumentoCliente.getValue());
            //cliente.setNroDocumento(txt_TipoCliente.getText());
            cliente.setNroDocumento(txt_nroDocumentoCliente.getText());
            cliente.setNroCelular(txt_celularCliente.getText());



            ClienteDao.crear(cliente);

        }else {
            cliente.setNombreCliente(txt_nombreCliente.getText());
            cliente.setApellidoPaternoCliente(txt_apellidoPaternoCliente.getText());
            cliente.setApellidoMaternoCliente(txt_apellidoMaternoCliente.getText());
            //cliente.setNroDocumento(txt_TipoCliente.getText());
            cliente.setTipoDocumento(cmb_tipoDocumentoCliente.getValue());
            cliente.setNroDocumento(txt_nroDocumentoCliente.getText());
            cliente.setNroCelular(txt_celularCliente.getText());

            ClienteDao.editar(cliente);
        }

        btn_cancelarCliente(actionEvent);
    }

    public void btn_cancelarCliente(ActionEvent actionEvent) {

        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

    //crear metodo setter con alt+ins de tipo cliente
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
        mostrarDatos();
    }

    private void mostrarDatos() {
        txt_IDCliente.setText(String.valueOf(cliente.getIdCliente()));
        txt_nombreCliente.setText(cliente.getNombreCliente());
        txt_apellidoPaternoCliente.setText(cliente.getApellidoPaternoCliente());
        txt_apellidoMaternoCliente.setText(cliente.getApellidoMaternoCliente());
        //txt_TipoCliente.setText(cliente.getTipoDocumento());
        txt_tipoDocumentoCliente.setText(cliente.getTipoDocumento());
        txt_nroDocumentoCliente.setText(cliente.getNroDocumento());
        txt_celularCliente.setText(cliente.getNroCelular());
    }


}
