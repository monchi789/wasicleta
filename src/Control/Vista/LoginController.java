package Control.Vista;

import Control.Dao.DaoUsuario.UsuarioDao;
import Control.encripta.Encripta;
import Modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {


    // Creamos una variable FXML de tipo TextField
    public TextField txtUsuario;

    // Creamos una variable FXML de tipo PasswordField
    public PasswordField txtPassword;

    private double posX = 0;
    private double posY = 0;

    String mostrarUsuario;

    public static String nombreUsuario;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {



    }

   public String setMostrarUsuario(){
        mostrarUsuario = txtUsuario.getText();
       return mostrarUsuario;
   }





    // Metodo del boton login para poder invocar al FXML principal
    public void Login(ActionEvent actionEvent) {



        //validacion del usuario

        Usuario usuario = UsuarioDao.buscarUsuario(txtUsuario.getText().trim());



        //creacion de nueva alerta
        Alert mensaje = new Alert(Alert.AlertType.WARNING);
        mensaje.setTitle("Autentificacion de usuario");
        mensaje.setHeaderText("Usuario o contraseña no validos");

        //validacion con un if y boolean
        if (usuario != null){

            boolean valida = usuario.getConstrasenaUsuario().equals(Encripta.encripta(txtPassword.getText()));

            if (valida){

                nombreUsuario = setMostrarUsuario();
                // System.out.println("h" + nombreUsuario);

                // mostrarUsuario = txtUsuario.getText();
                // System.out.println(setMostrarUsuario());

                try {
                    // Creamos un escenario
                    Stage stage = new Stage();

                    // Creamos una varible de tipo root y lo dejamos como null
                    Parent root = null;
                    // Creamos una variable root de tipo parent
                    root = FXMLLoader.load(getClass().getResource("/Vista/principal.fxml"));

                    // Creamos la escena
                    Scene scene = new Scene(root);


                    // Definimos un estilo de escenario sin fondo ni decoraciones
                    stage.initStyle(StageStyle.TRANSPARENT);

                    // El evento "setOnMousePressed" se llama solo cuando es presionado un boton del mouse
                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {

                            // Aqui asignamos la posicion actual que tiene la aplicacion en X y Y
                            posX = event.getSceneX();
                            posY = event.getSceneY();

                            // getSceneX() -> Returns horizontal position of the event relative to the origin of the Scene that contains the MouseEvent's source.
                            // getSceneY() -> Returns vertical position of the event relative to the origin of the Scene that contains the MouseEvent's source.

                        }
                    });

                    // El evento "setOnMouseDragged" se llama solo cuando el mouse se mueve con un boton presionado
                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {

                            // Asignamos la nueva posicion, obteniendo la posicion actual y restandolo con la posicion de inicio
                            stage.setX(event.getScreenX() - posX);
                            stage.setY(event.getScreenY() - posY);
                        }
                    });

                    // Colocanos la escena en el escenario
                    stage.setScene(scene);

                    // Mostramos la escena en pantalla
                    stage.show();




                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Invocamos el metodo para poder cerrar el login
                Close(actionEvent);

            }else {
                //mensaje
                mensaje.show();
            }

        }else {

            //mensaje
            mensaje.show();
        }

        /*
        // Creamos un escenario
        Stage stage = new Stage();

        // Creamos una varible de tipo root y lo dejamos como null
        Parent root = null;
        try {
            // Creamos una variable root de tipo parent
            root = FXMLLoader.load(getClass().getResource("/Vista/principal.fxml"));

            // Creamos la escena
            Scene scene = new Scene(root);

            // Definimos un estilo de escenario sin fondo ni decoraciones
            stage.initStyle(StageStyle.TRANSPARENT);

            // El evento "setOnMousePressed" se llama solo cuando es presionado un boton del mouse
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    // Aqui asignamos la posicion actual que tiene la aplicacion en X y Y
                    posX = event.getSceneX();
                    posY = event.getSceneY();

                    // getSceneX() -> Returns horizontal position of the event relative to the origin of the Scene that contains the MouseEvent's source.
                    // getSceneY() -> Returns vertical position of the event relative to the origin of the Scene that contains the MouseEvent's source.

                }
            });

            // El evento "setOnMouseDragged" se llama solo cuando el mouse se mueve con un boton presionado
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    // Asignamos la nueva posicion, obteniendo la posicion actual y restandolo con la posicion de inicio
                    stage.setX(event.getScreenX() - posX);
                    stage.setY(event.getScreenY() - posY);
                }
            });

            // Colocanos la escena en el escenario
            stage.setScene(scene);

            // Mostramos la escena en pantalla
            stage.show();




        } catch (IOException e) {
            e.printStackTrace();
        }

        // Invocamos el metodo para poder cerrar el login
        Close(actionEvent);
*/
    }

    // Creamos un metodo para poder cerrar la ventana de login
    public void Close(ActionEvent actionEvent) {

        // Metodo para poder cerrar el login
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();

    }

    // Metodo del boton CRUD para poder invocar al FXML del CRUD
    public void CRUD(ActionEvent actionEvent) {
      /*  //validacion del usuario para el crud

        Usuario usuario = UsuarioDao.buscarUsuario(txtUsuario.getText().trim());

        //creacion de nueva alerta
        Alert mensaje = new Alert(Alert.AlertType.WARNING);
        mensaje.setTitle("Autentificacion");
        mensaje.setHeaderText("Usuario o contraseña no validos");

        //validacion con un if y boolean
        if (usuario != null){

            boolean valida = usuario.getConstrasenaUsuario().equals(Encripta.encripta(txtPassword.getText()));

            if (valida){
                try {
                    // Creamos un escenario
                    Stage stage = new Stage();

                    // Creamos una varible de tipo root y lo dejamos como null
                    Parent root = null;
                    // Creamos una variable root de tipo parent
                    root = FXMLLoader.load(getClass().getResource("/Vista/crud/CRUDPrincipal.fxml"));

                    // Creamos la escena
                    Scene scene = new Scene(root);

                    // Definimos un estilo de escenario sin fondo ni decoraciones
                    stage.initStyle(StageStyle.TRANSPARENT);

                    // El evento "setOnMousePressed" se llama solo cuando es presionado un boton del mouse
                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {

                            // Aqui asignamos la posicion actual que tiene la aplicacion en X y Y
                            posX = event.getSceneX();
                            posY = event.getSceneY();

                            // getSceneX() -> Returns horizontal position of the event relative to the origin of the Scene that contains the MouseEvent's source.
                            // getSceneY() -> Returns vertical position of the event relative to the origin of the Scene that contains the MouseEvent's source.

                        }
                    });

                    // El evento "setOnMouseDragged" se llama solo cuando el mouse se mueve con un boton presionado
                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {

                            // Asignamos la nueva posicion, obteniendo la posicion actual y restandolo con la posicion de inicio
                            stage.setX(event.getScreenX() - posX);
                            stage.setY(event.getScreenY() - posY);
                        }
                    });

                    // Colocanos la escena en el escenario
                    stage.setScene(scene);

                    // Mostramos la escena en pantalla
                    stage.show();




                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Invocamos el metodo para poder cerrar el login
                Close(actionEvent);

            }else {
                //mensaje
                mensaje.show();
            }

        }else {

            //mensaje
            mensaje.show();
        }

*/
        try {
            // Creamos un escenario
            Stage stage = new Stage();

            // Creamos una varible de tipo root y lo dejamos como null
            Parent root = null;
            // Creamos una variable root de tipo parent
            root = FXMLLoader.load(getClass().getResource("/Vista/crud/CRUDPrincipal.fxml"));

            // Creamos la escena
            Scene scene = new Scene(root);

            // Definimos un estilo de escenario sin fondo ni decoraciones
            stage.initStyle(StageStyle.TRANSPARENT);

            // El evento "setOnMousePressed" se llama solo cuando es presionado un boton del mouse
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    // Aqui asignamos la posicion actual que tiene la aplicacion en X y Y
                    posX = event.getSceneX();
                    posY = event.getSceneY();

                    // getSceneX() -> Returns horizontal position of the event relative to the origin of the Scene that contains the MouseEvent's source.
                    // getSceneY() -> Returns vertical position of the event relative to the origin of the Scene that contains the MouseEvent's source.

                }
            });

            // El evento "setOnMouseDragged" se llama solo cuando el mouse se mueve con un boton presionado
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    // Asignamos la nueva posicion, obteniendo la posicion actual y restandolo con la posicion de inicio
                    stage.setX(event.getScreenX() - posX);
                    stage.setY(event.getScreenY() - posY);
                }
            });

            // Colocanos la escena en el escenario
            stage.setScene(scene);

            // Mostramos la escena en pantalla
            stage.show();




        } catch (IOException e) {
            e.printStackTrace();
        }

        // Invocamos el metodo para poder cerrar el login
        Close(actionEvent);

    }

}
