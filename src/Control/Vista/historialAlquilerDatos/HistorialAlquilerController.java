package Control.Vista.historialAlquilerDatos;

import Control.Dao.ClienteDao;
import Control.Dao.DaoBicicleta.BicicletaDao;

import Control.Dao.daoTipoBicicleta.TipoBicicletaDao;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class HistorialAlquilerController implements Initializable {
    public TableView tblHistorialAlquiler;
    public TableColumn columHistorialAlquilerMonto;
    public TableColumn columHistorialAlquilerComentarios;
    public TableColumn columHistorialAlquilerHoraEntrega;
    public TableColumn columHistorialAlquilerFecha;
    public TableColumn columHistorialAlquilerModelo;
    public TableColumn columHistorialAlquilerTelefono;
    public TableColumn columHistorialAlquilerNombre;

    @FXML
    private TextField txtBuscar;

    @FXML
    private Button btnBuscar;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*
        //creamos un metodo para llamar varios datos en este caso se llama cargarDatos
        cargarDatos();
        // Mostramos los datos de los setter de la clase HistorialALquiler
        columHistorialAlquilerMonto.setCellValueFactory(new PropertyValueFactory<>("montoDeAlquiler"));
        columHistorialAlquilerComentarios.setCellValueFactory(new PropertyValueFactory<>("comentarioDeAlquiler"));
        columHistorialAlquilerFecha.setCellValueFactory(new PropertyValueFactory<>("fechaDelAlquiler"));

        columHistorialAlquilerModelo.setCellValueFactory(new PropertyValueFactory<>("nombreCliente"));
        columHistorialAlquilerTelefono.setCellValueFactory(new PropertyValueFactory<>("nroCelular"));
        columHistorialAlquilerModelo.setCellValueFactory(new PropertyValueFactory<>("modeloBicicleta"));*/


    }

    @FXML
    void Buscar(ActionEvent event) {

    }

    /*private void cargarDatos() {
        // Limpiamos la lista para que no se agreguen nuevos datos
        tblHistorialAlquiler.getItems().clear();

        // Le pasamos al tableView los usuarios
        tblHistorialAlquiler.getItems().addAll(HistorialAlquilerDao.listar());

        tblHistorialAlquiler.getItems().addAll((ClienteDao.listar()));

        tblHistorialAlquiler.getItems().addAll((TipoBicicletaDao.listar()));


    }*/
}
