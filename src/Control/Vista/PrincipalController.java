package Control.Vista;

import Control.Dao.DaoUsuario.UsuarioDao;
import Modelo.Repositorio.Persistencia;
import Modelo.Usuario;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PrincipalController extends LoginController implements Initializable {



    public HBox HBoxPrincipal;
    public ToggleGroup principal;

    private double posX = 0;
    private double posY = 0;

    @FXML
    public Label lblUsuario;

    @FXML
    private Button btnUsuario;

    @FXML
    private Button btnSalir;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        lblUsuario.setText(nombreUsuario);

    }



    public void AlquilerBicicletas(ActionEvent actionEvent) {
        LoadPage(actionEvent, "alquiler");
    }
    public void HistorialDeAlquiler(ActionEvent actionEvent) {
        LoadPage(actionEvent, "historialAlquiler");
    }
    public void BicicletasAlquiladas(ActionEvent actionEvent) {
        LoadPage(actionEvent, "alquiladasBicicletas");
    }

    public void crearUsuario(ActionEvent actionEvent) {

        // lblUsuario.setText(nombreUsuario);

        LoadPage(actionEvent, "crearUsuario");
    }

    @FXML
    void UsuarioWatch(ActionEvent event) {
        LoadPage(event, "usuario");
    }

    @FXML
    void Salir(ActionEvent event) {
        try {
            Stage xd = new Stage();
            ((Stage)((Node)event.getSource()).getScene().getWindow()).close();
            Parent root  = FXMLLoader.load(getClass().getResource("/Vista/login.fxml"));
            Scene scene = new Scene(root);
            xd.initStyle(StageStyle.TRANSPARENT);
            scene.setFill(Color.TRANSPARENT);
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    posX = event.getSceneX();
                    posY = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xd.setX(event.getScreenX() - posX);
                    xd.setY(event.getScreenY() - posY);
                }
            });
            xd.setScene(scene);
            xd.show();
        }catch (IOException e){
        }
    }

    public void Close(ActionEvent event) {
        ((Stage)((Node)event.getSource()).getScene().getWindow()).close();
    }

    private void LoadPage(ActionEvent event, String page){
        if(HBoxPrincipal.getChildren().size() > 1){
            HBoxPrincipal.getChildren().remove(1);
        }
        if(((ToggleButton)event.getSource()).isSelected()){
            try {
                Parent root  = FXMLLoader.load(getClass().getResource("/Vista/" + page + ".fxml"));
                HBoxPrincipal.getChildren().add(root);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
