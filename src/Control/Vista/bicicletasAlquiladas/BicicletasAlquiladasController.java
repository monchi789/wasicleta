package Control.Vista.bicicletasAlquiladas;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class BicicletasAlquiladasController implements Initializable {

    @FXML
    private TextField txtBuscarBicicleta;

    @FXML
    private Button btnBuscarBicicleta;

    @FXML
    private TableView<?> tblBuscarBicicleta;

    @FXML
    private TableColumn<?, ?> columNombre;

    @FXML
    private TableColumn<?, ?> columNroCelular;

    @FXML
    private TableColumn<?, ?> columModelo;

    @FXML
    private TableColumn<?, ?> columFecha;

    @FXML
    private TableColumn<?, ?> columHoraAlquiler;

    @FXML
    private TableColumn<?, ?> columMonto;

    @FXML
    private TableColumn<?, ?> columEstado;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    void BuscarBicicleta(ActionEvent event) {

    }

}
